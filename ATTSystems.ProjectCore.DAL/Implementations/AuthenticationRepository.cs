﻿/* Copyright © ATT Systems Pte Ltd. All Rights Reserved
 * Project      : ATTSystems.ProjectCore.DAL.Implementations
 * Author       : Mark Tan
 * Created Date : 2019-06-07
 * Updated Date : 
 */


namespace ATTSystems.ProjectCore.DAL.Implementations
{
    using ATTSystems.Core;
    using ATTSystems.Core.Logger;
    using ATTSystems.Core.Model.DBModel;
    using ATTSystems.Core.Model.HttpModel;
    using ATTSystems.Core.Model.ViewModel;
    using ATTSystems.ProjectCore.DAL.Interfaces;
    using ATTSystems.Core.EntityFramework;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading.Tasks;

    public class AuthenticationRepository : IBaseRepository
    {
        private string ErrorMsg = string.Empty;

        public string GetErrorMsg()
        {
            return ErrorMsg;
        }

        public async Task<bool> ResetSecureCode(int rowId, string request)
        {
            ErrorMsg = string.Empty;
            bool result = false;

            using (var entity = new DataContext())
            {
                try
                {
                    JobRequest job = entity.JobRequests.FirstOrDefault(x => x.Id == rowId);

                    if (job != null)
                    {
                        int userId = 0;
                        int.TryParse(job.UserKey, out userId);
                        User user = entity.Users.FirstOrDefault(x => x.Id == userId);
                        if (user != null)
                        {
                            user.PasswordHash =CryptographyHash.HashPassword(request);
                            entity.SaveChanges();

                            job.JobStatus = 1;
                            job.UpdateBy = user.UserName;
                            job.UpdateDateTime = DateTime.Now;
                            entity.SaveChanges();
                            result = true;
                        }
                    }
                    else
                    {
                        ErrorMsg = "Invalid user";
                    }
                }
                catch (Exception ex)
                {
                    ErrorMsg = ex.Message;
                    if (ex.InnerException != null && !string.IsNullOrEmpty(ex.InnerException.Message))
                        ErrorMsg += ex.InnerException.Message;
                }
            }

            return await Task.FromResult<bool>(result);
        }

        public async Task<int> GetUserByRowId(string key)
        {
            ErrorMsg = string.Empty;
            int result = 0;
            using (var entity = new DataContext())
            {
                try
                {
                    DateTime now = DateTime.Now;
                    JobRequest job = entity.JobRequests.FirstOrDefault(x => x.JobKey == key && x.JobType == "RESETPWD");
                    if (job != null)
                    {
                        if (job.JobStatus == 0)
                        {
                            if (job.ExpiryDateTime > now)
                            {
                                result = job.Id;
                            }
                            else
                            {
                                ErrorMsg = "Reset job expired.";
                            }
                        }
                        else
                        {
                            ErrorMsg = "Reset job had been completed";
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorMsg = ex.Message;
                    if (ex.InnerException != null && !string.IsNullOrEmpty(ex.InnerException.Message))
                        ErrorMsg += ex.InnerException.Message;
                }
            }

            return await Task.FromResult<int>(result);
        }

        public async Task<List<Setting>> GetSettingByType(string type)
        {
            ErrorMsg = string.Empty;
            List<Setting> result = null;
            using (var entity = new DataContext())
            {
                try
                {
                    var list = entity.Settings.Where(x => x.Type == type);
                    if (list != null)
                    {
                        result = list.ToList();
                    }
                }
                catch (Exception ex)
                {
                    ErrorMsg = ex.Message;
                    if (ex.InnerException != null && !string.IsNullOrEmpty(ex.InnerException.Message))
                        ErrorMsg += ex.InnerException.Message;
                }
            }

            return await Task.FromResult<List<Setting>>(result);
        }

        public async Task<bool> RequestAccountReset(APIRequest request)
        {
            ErrorMsg = string.Empty;
            bool result = false;
            using (var entity = new DataContext())
            {
                
                try
                {
                    User user = entity.Users.FirstOrDefault(x => x.Email.ToUpper() == request.RequestString.ToUpper() && x.IsDeleted == false);
                    if (user != null)
                    {
                        string[] tok = request.RequestString.Split('@');
                        string reqUser = tok[0].Length > 50 ? tok[0].Substring(0, 50) : tok[0];
                        DateTime now = DateTime.Now;
                        string key = string.Format("{0}{1}{2}", now.ToString("HHmmss"), user.UserName, now.ToString("yyyyMMdd"));
                        key = CryptographyHash.ConvertToSHA256(key);
                        entity.JobRequests.Add(new JobRequest
                        {
                            UserId = 0,
                            UserKey = user.Id.ToString(),
                            JobType = "RESETPWD",
                            JobKey = key,
                            JobStatus = 0,
                            JobMessage = "NA",
                            ExpiryDateTime = now.AddMinutes(15),
                            CreateDateTime = now,
                            CreateBy = reqUser,
                        });

                        entity.SaveChanges();
                        ErrorMsg = key;
                        result = true;
                    }
                    else
                    {
                        ErrorMsg = "Invalid User Account.";
                    }
                }
                catch (Exception ex)
                {
                    ErrorMsg = ex.Message;
                    if (ex.InnerException != null && !string.IsNullOrEmpty(ex.InnerException.Message))
                        ErrorMsg += ex.InnerException.Message;
                }
            }

            return await Task.FromResult<bool>(result);
        }

        public async Task<User> GetUserAsync(string userName)
        {
            User result = null;
            using (var entity = new DataContext())
            {
                try
                {
                    result = entity.Users.Include(x => x.Departments).FirstOrDefault(x => x.UserName.ToUpper() == userName.ToUpper() && x.IsDeleted == false);

                    // sort Department list
                    if (result.Departments != null && result.Departments.Count() > 0)
                    {
                        var dept = result.Departments.Where(x => x.IsDeleted == false);
                        if (dept != null && dept.Count() > 0)
                        {
                            result.Departments = dept.ToList();
                        }
                        else
                        {
                            result.Departments = null;
                        }
                    }

                    // sort Role List
                    var userRole = entity.Users.Include(x => x.Roles).FirstOrDefault(x => x.UserName.ToUpper() == userName.ToUpper() && x.IsDeleted == false);
                    if (userRole != null && userRole.Roles != null && userRole.Roles.Count > 0)
                    {
                        var userRoleList = userRole.Roles.Where(x => x.IsDeleted == false);

                        if (userRoleList != null && userRoleList.Count() > 0)
                        {
                            result.Roles = userRoleList.ToList();
                        }
                    }

                    // sort ModuleList
                    if (result.Roles != null && result.Roles.Count > 0)
                    {
                        List<int> roleIds = result.Roles.Select(x => x.Id).ToList();
                        if (roleIds != null && roleIds.Count > 0)
                        {
                            var newRoles = entity.Roles.Include(x => x.Modules).Where(x => roleIds.Contains(x.Id));
                            if (newRoles != null && newRoles.Count() > 0)
                            {
                                result.ModuleList = new List<Module>();
                                foreach (Role role in newRoles)
                                {
                                    if (role.Modules != null && role.Modules.Count > 0)
                                    {
                                        var tmp = role.Modules.Where(x => x.IsDeleted == false);
                                        if (tmp != null && tmp.Count() > 0)
                                        {
                                            result.ModuleList.AddRange(tmp.ToList());
                                        }
                                    }
                                }

                                if (result.ModuleList.Count == 0)
                                {
                                    result.ModuleList = null;
                                }
                            }

                        }
                    }

                }
                catch (Exception ex)
                {
                    if (!string.IsNullOrEmpty(ex.InnerException.Message) && ex.InnerException.Message.Contains(" error occurred while establishing a connection to SQL Server"))
                    {
                        ErrorMsg = " Error occurred while establishing a connection to SQL Server";
                    }
                    else
                    {
                        ErrorMsg = ex.Message;
                    }
                }
            }

            return await Task.FromResult<User>(result);
        }

        public async Task<string> ChangePasswordAsync(string userName, string hashPassword)
        {
            string result = null;

            using (var entity = new DataContext())
            {
                try
                {
                    User user = entity.Users.FirstOrDefault(x => x.UserName.ToLower() == userName.ToLower());
                    if (user != null)
                    {
                        user.PasswordHash = hashPassword;
                        user.UpdateDateTime = DateTime.Now;
                        user.UpdateBy = userName;
                        entity.SaveChanges();
                    }
                    else
                    {
                        result = "User not found.";
                    }
                }
                catch (Exception ex)
                {
                    if (!string.IsNullOrEmpty(ex.InnerException.Message) && ex.InnerException.Message.Contains(" error occurred while establishing a connection to SQL Server"))
                    {
                        ErrorMsg = " Error occurred while establishing a connection to SQL Server";
                    }
                    else
                    {
                        ErrorMsg = ex.Message;
                    }
                }
            }

            return await Task.FromResult<string>(result);
        }

        public async Task<List<ModuleViewModel>> GetModuleList(APIRequest request)
        {
            ErrorMsg = string.Empty;
            List<ModuleViewModel> result = new List<ModuleViewModel>();

            string userName = request.UserName;
            using (var entity = new DataContext())
            {
                User user = entity.Users.Include(x => x.Roles).FirstOrDefault(x => x.UserName.ToLower() == userName.ToLower() && x.IsDeleted == false);
                if (user != null && user.Roles != null && user.Roles.Count > 0)
                {
                    List<Role> roleList = user.Roles.ToList();
                    foreach (Role role in roleList)
                    {
                        IQueryable<Role> queryRoleList = entity.Roles.Include(x => x.Modules).Where(x => x.Id == role.Id && x.IsDeleted == false);
                        if (queryRoleList != null && queryRoleList.Count() > 0)
                        {
                            List<Role> qRoleList = queryRoleList.ToList();
                            foreach (Role queryRole in qRoleList)
                            {
                                if (queryRole.Modules != null && queryRole.Modules.Count > 0)
                                {
                                    foreach (Module mod in queryRole.Modules)
                                    {
                                        ModuleViewModel existModule = result.FirstOrDefault(x => x.Id == mod.Id);
                                        if (!mod.IsDeleted && existModule == null)
                                        {
                                            result.Add(new ModuleViewModel
                                            {
                                                Id = mod.Id,
                                                IsSelected = true,
                                                ModuleId = mod.ModuleId,
                                                ModuleName = mod.ModuleName
                                            });
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return await Task.FromResult<List<ModuleViewModel>>(result);
        }
    }
}
