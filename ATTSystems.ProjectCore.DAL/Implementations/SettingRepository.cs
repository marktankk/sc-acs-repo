﻿/* Copyright © ATT Systems Pte Ltd. All Rights Reserved
 * Project      : ATTSystems.ProjectCore.DAL.Implementations
 * Author       : Mark Tan
 * Created Date : 2019-06-07
 * Updated Date : 
 */


namespace ATTSystems.ProjectCore.DAL.Implementations
{
    using ATTSystems.Core;
    using ATTSystems.Core.Model.DBModel;
    using ATTSystems.Core.Model.HttpModel;
    using ATTSystems.Core.Model.ViewModel;
    using ATTSystems.Core.Utilities;
    using ATTSystems.Core.EntityFramework;
    using ATTSystems.ProjectCore.DAL.Interfaces;
    
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading.Tasks;
    public class SettingRepository : IBaseRepository
    {
        private string ErrorMsg = string.Empty;

        public string GetErrorMsg()
        {
            return ErrorMsg;
        }

        public async Task<User> GetUserAsync(int id)
        {
            ErrorMsg = string.Empty;
            User user = null;

            using (var entity = new DataContext())
            {
                try
                {
                    user = entity.Users.Include(x => x.Departments).Include(x => x.Roles).FirstOrDefault(x => x.Id == id && x.IsDeleted == false);

                    // clean up the deleted roles
                    if (user.Roles != null && user.Roles.Count > 0)
                    {
                        var roleList = user.Roles.Where(x => x.IsDeleted == false);
                        if (roleList != null)
                        {
                            user.Roles = roleList.ToList();
                        }
                    }

                    // Clean up the deleted Department
                    if (user.Departments != null)
                    {
                        var deptList = user.Departments.Where(x => x.IsDeleted == false);
                        if (deptList != null)
                        {
                            user.Departments = deptList.ToList();
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorMsg = ex.Message;
                }

            }

            return await Task.FromResult<User>(user);
        }

        public async Task<string> UpdateRoleAsync(APIRequest request)
        {
            ErrorMsg = string.Empty;
            string roleId = "0";

            RoleViewModel model = JsonConvert.DeserializeObject<RoleViewModel>(request.Model.ToString());

            using (var entity = new DataContext())
            {
                Role role = entity.Roles.FirstOrDefault(x => x.Id != model.Id && x.Name == model.Name && x.IsDeleted == false);

                if (role == null)
                {
                    role = entity.Roles.FirstOrDefault(x => x.Id == model.Id);

                    if (role != null)
                    {
                        var moduleIdList = model.ModuleList.Select(x => x.Id);
                        var moduleList = entity.Modules.Where(x => moduleIdList.Contains(x.Id));
                        if (moduleList != null && moduleList.Count() > 0)
                        {
                            role.Modules.Clear();
                            string allowableModule = string.Empty;

                            foreach (Module module in moduleList)
                            {
                                role.Modules.Add(module);
                                allowableModule = string.Format("{0}, {1}", allowableModule, module.ModuleId);
                            }

                            role.AllowPermission = string.IsNullOrEmpty(allowableModule) ? string.Empty : allowableModule.Substring(2);
                            role.Name = model.Name;
                            role.Description = model.Description;
                            role.UpdateBy = request.UserName;
                            role.UpdateDateTime = DateTime.Now;

                            entity.SaveChanges();
                            roleId = role.Id.ToString();
                        }
                        else
                        {
                            ErrorMsg = "Module not found in the system.";
                        }
                    }
                    else
                    {
                        ErrorMsg = "Role not found in the system.";
                    }
                }
                else
                {
                    ErrorMsg = "Same Role Name found Role in the system.";
                }
            }

            return await Task.FromResult<string>(roleId);
        }

        public async Task<int> DeleteUserAsync(APIRequest request)
        {
            ErrorMsg = string.Empty;
            int result = 0;

            using (var entity = new DataContext())
            {
                try
                {
                    string[] idArr = request.RequestString.Split(',');
                    foreach (string id in idArr)
                    {
                        int userId = 0;
                        int.TryParse(id, out userId);
                        User user = entity.Users.FirstOrDefault(x => x.Id == userId);
                        if (user != null)
                        {
                            user.IsDeleted = true;
                            user.UpdateBy = request.UserName;
                            user.UpdateDateTime = DateTime.Now;
                            entity.SaveChanges();
                            result += 1;
                        }
                        else
                        {
                            result = -1;
                            ErrorMsg = "User not exist in the system.";
                        }
                    }
                }
                catch (Exception ex)
                {
                    // write to log
                    ErrorMsg = ex.Message;
                    ErrorMsg = "System internal error.";

                    if (result > 0)
                    {
                        ErrorMsg = string.Format("{0} Partial deleted [{1}]", ErrorMsg, result);
                    }
                    else
                    {
                        result = -1;
                    }
                }
            }

            return await Task.FromResult<int>(result);
        }

        public async Task<int> UpdateUserAsync(APIRequest request)
        {
            ErrorMsg = string.Empty;
            int roleId = 0;
            UserViewModel model = JsonConvert.DeserializeObject<UserViewModel>(request.Model.ToString());
            using (var entity = new DataContext())
            {
                try
                {
                    User user = entity.Users.FirstOrDefault(x => x.UserName.ToLower() == model.UserName.ToLower() && x.Id != model.Id && x.IsDeleted == false);

                    if (user == null) // no duplicate User name
                    {
                        user = entity.Users.Include(x => x.Roles).Include(x => x.Departments).FirstOrDefault(x => x.Id == model.Id && x.IsDeleted == false);
                        if (user != null)
                        {
                            user.UserName = model.UserName;
                            user.Email = model.Email;
                            user.UpdateBy = request.UserName;
                            user.UpdateDateTime = DateTime.Now;

                            var roleIdList = model.RoleList.Select(x => x.Id);
                            var roleList = entity.Roles.Where(x => roleIdList.Contains(x.Id));
                            if (roleList != null)
                            {
                                user.Roles.Clear();
                                foreach (Role role in roleList)
                                {
                                    user.Roles.Add(role);
                                }
                            }

                            var deptIdList = model.DepartmentList.Select(x => x.Department_ID);
                            var deptList = entity.Departments.Where(x => deptIdList.Contains(x.Department_ID));
                            if (deptList != null)
                            {
                                user.Departments.Clear();
                                foreach (Department dept in deptList)
                                {
                                    user.Departments.Add(dept);
                                }
                            }

                            entity.SaveChanges();
                            roleId = 1;
                        }
                        else
                        {
                            roleId = -1;
                            ErrorMsg = "User not exist in the system.";
                        }
                    }
                    else
                    {
                        roleId = -2;
                        ErrorMsg = "Same User name exist in the system.";
                    }
                }
                catch (Exception ex)
                {
                    roleId = 0;
                    ErrorMsg = string.Format("System internal error.\n{0}", ex.Message);
                    // write to log
                }
            }

            return await Task.FromResult<int>(roleId);
        }

        public async Task<int> AddUserAsync(APIRequest request)
        {
            ErrorMsg = string.Empty;
            int roleId = 0;

            UserViewModel model = JsonConvert.DeserializeObject<UserViewModel>(request.Model.ToString());

            using (var entity = new DataContext())
            {
                try
                {
                    User user = entity.Users.FirstOrDefault(x => x.UserName.ToLower() == model.UserName.ToLower() && x.IsDeleted == false);

                    if (user == null)
                    {


                        user = new User
                        {
                            UserName = model.UserName,
                            Email = model.Email,
                            EmailConfirmed = true,
                            PasswordHash = CryptographyHash.HashPassword(model.Password),
                            SecurityStamp = string.Empty,
                            PhoneNumber = "0",
                            PhoneNumberConfirmed = false,
                            TwoFactorEnabled = false,
                            LockoutEndDateUtc = null,
                            LockoutEnabled = false,
                            AccessFailedCount = 0,
                            IsDeleted = false,
                            CreateBy = request.UserName,
                            CreateDateTime = DateTime.Now,
                            UpdateBy = string.Empty,
                            UpdateDateTime = null,
                        };
                        entity.Users.Add(user);
                        entity.SaveChanges();

                        var roleIdList = model.RoleList.Select(x => user.Id);
                        var roleList = entity.Roles.Where(x => roleIdList.Contains(x.Id));
                        if (roleList != null)
                        {
                            foreach (Role role in roleList)
                            {
                                user.Roles.Add(role);
                            }
                        }
                        entity.SaveChanges();

                        var deptIdList = model.DepartmentList.Select(x => x.Department_ID);
                        var deptList = entity.Departments.Where(x => deptIdList.Contains(x.Department_ID));
                        if (deptList != null)
                        {
                            foreach (Department dept in deptList)
                            {
                                user.Departments.Add(dept);
                            }
                        }

                        entity.SaveChanges();
                        roleId = 1;
                    }
                    else
                    {
                        roleId = -1;
                        ErrorMsg = "User name exist in the system.";
                    }
                }
                catch (Exception ex)
                {
                    roleId = 0;
                    ErrorMsg = string.Format("System internal error.\n{0}", ex.Message);
                    // write to log
                }
            }

            return await Task.FromResult<int>(roleId);
        }

        public async Task<string> AddRoleAsync(APIRequest request)
        {
            ErrorMsg = string.Empty;
            string roleId = "0";

            RoleViewModel model = JsonConvert.DeserializeObject<RoleViewModel>(request.Model.ToString());

            using (var entity = new DataContext())
            {
                try
                {
                    Role role = entity.Roles.FirstOrDefault(x => x.Name == model.Name && x.IsDeleted == false);

                    if (role == null)
                    {

                        var moduleIdList = model.ModuleList.Select(x => x.Id);
                        var moduleList = entity.Modules.Where(x => moduleIdList.Contains(x.Id));

                        if (moduleList != null && moduleList.Count() > 0)
                        {                            
                            role = new Role
                            {
                                Name = model.Name,
                                CreateBy = request.UserName,
                                CreateDateTime = DateTime.Now,
                                Description = model.Description,
                                IsDeleted = false,
                                UpdateBy = string.Empty,
                                UpdateDateTime = null,
                            };

                            string allowableModule = string.Empty;
                            foreach (Module module in moduleList)
                            {
                                role.Modules.Add(module);
                                allowableModule = string.Format("{0}, {1}", allowableModule, module.ModuleId);
                            }

                            role.AllowPermission = string.IsNullOrEmpty(allowableModule) ? string.Empty : allowableModule.Substring(2);


                            User user = entity.Users.FirstOrDefault(x => x.UserName.ToLower() == request.UserName.ToLower());
                            if (user != null)
                            {
                                role.Users.Add(user);
                            }

                            if (request.UserName.ToLower() != "administrator") // create a new record for administrator
                            {
                                User adminUser = entity.Users.FirstOrDefault(x => x.UserName.ToLower() == request.UserName.ToLower());
                                if (adminUser != null)
                                {
                                    role.Users.Add(adminUser);
                                }
                            }

                            entity.Roles.Add(role);
                            entity.SaveChanges();

                        }
                        else
                        {
                            ErrorMsg = "No Module found in the system.";
                        }
                    }
                    else
                    {
                        ErrorMsg = "Role name exist in the system.";
                    }
                }
                catch (Exception ex)
                {
                    roleId = "0";
                    ErrorMsg = string.Format("System internal error.\n{0}", ex.Message);
                    // write to log
                }
            }

            return await Task.FromResult<string>(roleId);
        }

        public async Task<List<Module>> GetModuleListAsync()
        {
            List<Module> result = null;
            using (var entity = new DataContext())
            {
                try
                {
                    var modules = entity.Modules.Where(x => x.IsDeleted == false);
                    result = modules != null ? modules.ToList() : null;
                }
                catch (Exception ex)
                {
                    ErrorMsg = ex.Message;
                }
            }

            return await Task.FromResult<List<Module>>(result);
        }

        public async Task<Role> GetRoleAsync(int id)
        {
            Role result = null;
            using (var entity = new DataContext())
            {
                try
                {
                    result = entity.Roles.Include(x => x.Modules).FirstOrDefault(x => x.Id == id);
                }
                catch (Exception ex)
                {
                    ErrorMsg = ex.Message;
                }
            }

            return await Task.FromResult<Role>(result);
        }

        public async Task<List<Role>> GetRoleListAsync(string userName)
        {
            List<Role> result = null;
            using (var entity = new DataContext())
            {
                try
                {
                    User user = entity.Users.Include(y => y.Roles).FirstOrDefault(x => x.UserName.ToLower() == userName.ToLower());
                    if (user.Roles != null)
                    {
                        var roleList = user.Roles.Where(x => x.IsDeleted == false);
                        result = roleList != null ? roleList.ToList() : null;
                    }
                }
                catch (Exception ex)
                {
                    ErrorMsg = ex.Message;
                }
            }

            return await Task.FromResult<List<Role>>(result);
        }

        public async Task<List<Department>> GetDepartmentListAsync(string userName)
        {
            List<Department> result = null;

            using (var entity = new DataContext())
            {
                try
                {
                    User user = entity.Users.Include(x => x.Departments).FirstOrDefault(x => x.UserName.ToLower() == userName.ToLower() && x.IsDeleted == false);

                    if (user.Departments != null)
                    {
                        // filter department that is not deleted
                        var deptList = user.Departments.Where(x => x.IsDeleted == false);
                        result = deptList != null ? deptList.ToList() : null;
                    }
                }
                catch (Exception ex)
                {
                    ErrorMsg = ex.Message;
                }
            }

            return await Task.FromResult<List<Department>>(result);

        }

        public async Task<Department> GetDepartmentAsync(int id)
        {
            Department result = null;
            using (var entity = new DataContext())
            {
                result = entity.Departments.FirstOrDefault(x => x.Department_ID == id);
            }

            return await Task.FromResult<Department>(result);
        }

        public async Task<int> UpdateDepartmentAsync(APIRequest request)
        {
            ErrorMsg = string.Empty;
            int deptId = 0;

            DepartmentViewModel model = JsonConvert.DeserializeObject<DepartmentViewModel>(request.Model.ToString());

            using (var entity = new DataContext())
            {
                try
                {
                    Department dept = entity.Departments.FirstOrDefault(x => x.Department_ID != model.Department_ID && x.Department_Name == model.Department_Name);
                    if (dept == null)
                    {
                        dept = entity.Departments.FirstOrDefault(x => x.Department_ID == model.Department_ID);

                        if (dept != null)
                        {
                            dept.Department_Name = model.Department_Name;
                            dept.Description = model.Description;
                            dept.UpdateDateTime = DateTime.Now;
                            dept.UpdateBy = request.UserName;
                            entity.SaveChanges();
                            deptId = dept.Department_ID;
                        }
                        else
                        {
                            ErrorMsg = "Department not found in system.";
                        }
                    }
                    else
                    {
                        ErrorMsg = "Same department name found in system.";
                    }

                }
                catch (Exception ex)
                {
                    ErrorMsg = ex.Message;
                    //write to log
                    deptId = -1;
                    ErrorMsg = "System internal error.";
                }
            }

            return await Task.FromResult<int>(deptId);
        }

        public async Task<int> AddDepartmentAsync(APIRequest request)
        {
            ErrorMsg = string.Empty;
            int deptId = 0;

            DepartmentViewModel model = JsonConvert.DeserializeObject<DepartmentViewModel>(request.Model.ToString());

            using (var entity = new DataContext())
            {
                try
                {
                    Department dept = entity.Departments.FirstOrDefault(x => x.Department_Name.ToLower() == model.Department_Name.ToLower() && x.IsDeleted == false);

                    if (dept == null)
                    {
                        User user = entity.Users.FirstOrDefault(x => x.UserName.ToLower() == request.UserName.ToLower());
                        if (user != null)
                        {
                            // Generate 
                            deptId = entity.Departments.Any() ? entity.Departments.Select(x => x.Department_ID).Max() + 1 : 1;

                            dept = new Department
                            {
                                Department_ID = deptId,
                                Description = model.Description,
                                Department_Name = model.Department_Name,
                                IsDeleted = false,
                                UpdateDateTime = null,
                                CreateDateTime = DateTime.Now,
                                CreateBy = string.IsNullOrEmpty(request.UserName) ? "" : request.UserName,
                            };

                            dept.Users.Add(user);

                            if (request.UserName.ToLower() != "administrator") // create a new record for administrator
                            {
                                User adminUser = entity.Users.FirstOrDefault(x => x.UserName.ToLower() == request.UserName.ToLower());
                                if (adminUser != null)
                                {
                                    dept.Users.Add(adminUser);
                                }
                            }

                            entity.Departments.Add(dept);
                            entity.SaveChanges();
                        }
                        else
                        {
                            ErrorMsg = string.Format("System did not find user with username = {0}", request.UserName);
                        }
                    }
                    else
                    {
                        ErrorMsg = "System found similar department name.";
                    }
                }
                catch (Exception ex)
                {
                    deptId = -1;
                    ErrorMsg = string.Format("System internal error.\n{0}", ex.Message);
                    // write to log
                }
            }

            return await Task.FromResult<int>(deptId);
        }

        public async Task<int> DeleteRoleAsync(APIRequest request)
        {
            ErrorMsg = string.Empty;
            int result = 0;

            using (var entity = new DataContext())
            {
                try
                {
                    string[] idArr = request.RequestString.Split(',');

                    foreach (string id in idArr)
                    {
                        int roleId = 0;
                        int.TryParse(id, out roleId);
                        Role role = entity.Roles.FirstOrDefault(x => x.Id == roleId);
                        if (role != null)
                        {
                            role.IsDeleted = true;
                            role.UpdateDateTime = DateTime.Now;
                            role.UpdateBy = request.UserName;
                            entity.SaveChanges();
                            result += 1;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorMsg = ex.Message;
                    // write to log
                    ErrorMsg = "System internal error.";

                    if (result > 0)
                    {
                        ErrorMsg = string.Format("{0} Partial deleted [{1}]", ErrorMsg, result);
                    }
                    else
                    {
                        result = -1;
                    }
                }

                return await Task.FromResult<int>(result);
            }
        }

        public async Task<int> DeleteDepartmentAsync(APIRequest request)
        {
            ErrorMsg = string.Empty;
            int result = 0;

            using (var entity = new DataContext())
            {
                try
                {
                    string[] idArr = request.RequestString.Split(',');

                    foreach (string id in idArr)
                    {
                        int deptId = 0;

                        if (int.TryParse(id, out deptId) && deptId > 0)
                        {
                            Department dept = entity.Departments.FirstOrDefault(x => x.Department_ID == deptId);
                            if (dept != null)
                            {
                                dept.IsDeleted = true;
                                dept.UpdateDateTime = DateTime.Now;
                                dept.UpdateBy = request.UserName;
                                entity.SaveChanges();
                                result += 1;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorMsg = ex.Message;
                    // write to log
                    ErrorMsg = "System internal error.";

                    if (result > 0)
                    {
                        ErrorMsg = string.Format("{0} Partial deleted [{1}]", ErrorMsg, result);
                    }
                    else
                    {
                        result = -1;
                    }
                }

                return await Task.FromResult<int>(result);
            }
        }

        public async Task<List<User>> GetUserListAsync(string userName)
        {
            List<User> result = null;
            using (var entity = new DataContext())
            {
                try
                {
                    var list = entity.Users.Include(x => x.Departments).Include(x => x.Roles).Where(x => x.IsDeleted == false);
                    if (list != null)
                    {
                        result = list.ToList();
                    }
                }
                catch (Exception ex)
                {
                    ErrorMsg = ex.Message;
                }
            }

            return await Task.FromResult<List<User>>(result);
        }

        public async Task<List<Department>> GetActiveDepartmentListAsync()
        {
            List<Department> result = null;

            using (var entity = new DataContext())
            {
                try
                {
                    var list = entity.Departments.Where(x => x.IsDeleted == false);
                    if (list != null)
                    {
                        result = list.ToList();
                    }
                }
                catch (Exception ex)
                {
                    ErrorMsg = ex.Message;
                }
            }

            return await Task.FromResult<List<Department>>(result);
        }

        public async Task<List<Role>> GetActiveRoleListAsync()
        {
            List<Role> result = null;
            using (var entity = new DataContext())
            {
                try
                {
                    var roleList = entity.Roles.Where(x => x.IsDeleted == false);
                    if (roleList != null)
                    {
                        result = roleList.ToList();
                    }
                }
                catch (Exception ex)
                {
                    ErrorMsg = ex.Message;
                }
            }

            return await Task.FromResult<List<Role>>(result);
        }
    }
}
