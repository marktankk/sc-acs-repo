﻿using ATTSystems.Core.EntityFramework;


namespace ATTSystems.ProjectCore.DAL
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    

    public class DataContext : DBCtx
    {
        public DataContext() : base()
        {
            
        }

    

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Place your Entity Framework Model here


        }
    }
}