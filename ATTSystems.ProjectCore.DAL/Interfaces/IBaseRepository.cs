﻿/* Copyright © ATT Systems Pte Ltd. All Rights Reserved
 * Project      : ATTSystems.ProjectCore.DAL.Interfaces
 * Author       : Mark Tan
 * Created Date : 2019-09-29
 * Updated Date : 
 */

namespace ATTSystems.ProjectCore.DAL.Interfaces
{
    interface IBaseRepository
    {
        string GetErrorMsg();
    }
}
