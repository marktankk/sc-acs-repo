﻿/* Copyright © ATT Systems Pte Ltd. All Rights Reserved
 * Project      : ATTSystems.ProjectCore
 * Author       : Mark Tan
 * Created Date : 2019-06-07
 * Updated Date : 
 */

namespace ATTSystems.ProjectCore.Controllers
{
    using ATTSystems.Core.Logger;
    using ATTSystems.Core.Model.HttpModel;
    using ATTSystems.Core.Model.ViewModel;
    using ATTSystems.ProjectCore.Helper;
    using ATTSystems.ProjectCore.Model.ViewModel;
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Security;

    [LocalAuthorize]
    public class HomeController : Controller
    {
        // GET: Home
        [NoCache]
        public ActionResult Index()
        {
            List<ModuleViewModel> model = new List<ModuleViewModel>();
            try
            {
                ViewBag.Session = System.Web.HttpContext.Current.Session.Timeout * 60;

                if (Session["MODULE"] != null)
                {
                    string moduleList = Session["MODULE"].ToString();
                    if (!string.IsNullOrEmpty(moduleList))
                    {
                        string[] tok = moduleList.Split(',');
                        foreach (string item in tok)
                        {
                            model.Add(new ModuleViewModel
                            {
                                ModuleId = item,
                                ModuleName = item,
                                Id = 0
                            });
                        }
                    }

                    ViewBag.Title = "Project Home Page";
                    ViewBag.CurrentUserName = Session["USERKEY"]; // ticket.Name;
                }
                else
                {
                    RedirectToAction("Expired", "Auth");
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.LogError(ex);
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult Dashboard()
        {
            DashboardViewModel model = new DashboardViewModel();

            try
            {
                // Make API Call  - sample :- 
                //APIRequest req = new APIRequest
                //{
                //    RequestType = Session["Role"] != null ? Session["Role"].ToString() : "",
                //    RequestString = "GetDashboard",
                //    UserName = Session["USERKEY"] != null ? Session["USERKEY"].ToString() : "",
                //    Message = string.Empty,
                //};

                //var resp = await WebAPIHelper.AppRequestAsync("/Home/GetDashboard/", req);
                //if (resp != null && resp.DashboardView != null)
                //{
                //    model = resp.DashboardView;
                //}
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(ex.Message);
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Error in processing dashboard.");
            }

            return View(model);
        }
        public ActionResult Error(string aspxerrorpath)
        {
            return View();
        }
    }
}