﻿/* Copyright © ATT Systems Pte Ltd. All Rights Reserved
 * Project      : ATTSystems.ProjectCore
 * Author       : Mark Tan
 * Created Date : 2019-06-07
 * Updated Date : 2019-07-22
 * 
 * Description  :
 * Include Single Signed on for MAS user
 */

namespace ATTSystems.ProjectCore.Controllers
{
    using System;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Security;
    using Newtonsoft.Json;
    using ATTSystems.Core.Logger;
    using ATTSystems.Core.Model.ViewModel;
    using System.Configuration;
    using ATTSystems.Core.Model.HttpModel;
    using System.IO;
    using System.Drawing;
    using System.Drawing.Text;
    using System.Drawing.Drawing2D;
    using System.Net;
    using System.Linq;
    using ATTSystems.ProjectCore.Helper;

    public class AuthController : Controller
    {
        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            //var name = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            //name += " | IsAuthenticated=" + User.Identity.IsAuthenticated.ToString();
            //name += " | AuthenticationType=" + User.Identity.AuthenticationType;
            //name += " | Name=" + User.Identity.Name;
            //ViewBag.AdUserName = name;
            
            return View();
        }

        [AllowAnonymous]
        public ActionResult RequestReset()
        {
            RequestResetCodeViewModel model = new RequestResetCodeViewModel {
                Email = string.Empty,
                Captcha = string.Empty,
                CaptchaImage = SetupCaptchaBase64Image(),
            };
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult RefreshCaptchaImage(bool noisy = true)
        {
            dynamic showMessageString = string.Empty;

            string captchaContent = SetupCaptchaBase64Image();
            if (!string.IsNullOrEmpty(captchaContent))
            {
                showMessageString = new
                {
                    Code = 1,
                    Content = captchaContent,
                };
            }
            else
            {
                showMessageString = new
                {
                    Code = 1,
                    Content = "Fail to get Captcha Image",
                };
            }

            return Json(showMessageString, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult RequestResetSuccess()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Expired()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> RequestAccountReset(RequestResetCodeViewModel model)
        {
            dynamic showMessageString = string.Empty;
            try
            {
                // check for Captcha
                if (Session["captchasessionvariable"] == null || string.IsNullOrEmpty(model.Captcha) ||
                   Session["captchasessionvariable"].ToString().ToUpper() != model.Captcha.ToUpper())
                {
                    string cap = Session["captchasessionvariable"].ToString();

                    LoggerHelper.Instance.TraceLog("RequestAccountReset - Invalid CaptCha");
                    showMessageString = new
                    {
                        Code = 203,
                        Message = "Invalid Captcha. Please refresh and re-enter Captcha code.",
                    };

                    Session["captchasessionvariable"] = "";
                    return Json(showMessageString, JsonRequestBehavior.AllowGet);
                }

                string errmsg = string.Empty;
                if (ModelState.IsValid && model != null)
                {
                    Session["captchasessionvariable"] = "";
                    APIRequest req = new APIRequest
                    {
                        RequestType = "RequestAccountReset",
                        RequestString = model.Email,
                        Message = model.Email

                    };

                    var resp = await WebAPIHelper.APIRequestAsync("/Auth/RequestAccountReset/", req);
                    if (resp != null && resp.Succeeded)
                    {
                        showMessageString = new
                        {
                            Code = 200,
                            Message = "Success",
                        };
                    }
                    else
                    {
                        string msg = "Failed ";
                        if (resp != null && !string.IsNullOrEmpty(resp.Message))
                        {
                            msg += "- " + resp.Message;
                        }

                        showMessageString = new
                        {
                            Code = 202,
                            Message = msg,
                        };
                    }

                }
                else
                {
                    // validate by Model state
                    foreach (string key in ViewData.ModelState.Keys)
                    {
                        string modelStateError = string.Empty;
                        var modelState = ViewData.ModelState[key];
                        if (modelState.Errors != null && modelState.Errors.Count > 0)
                        {
                            errmsg = string.Format(@"{0}<br />{1}:{2}", errmsg, key, modelState.Errors.FirstOrDefault().ErrorMessage);
                        }
                    }

                    showMessageString = new
                    {
                        Code = 201, // invalid format
                        Message = !string.IsNullOrEmpty(errmsg) ? errmsg.Substring(6) : errmsg,
                        ModalType = "Add",
                    };
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Controller: Auth, Action: RequestAccountReset, Message: {0} ", ex.Message));
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Error in processing account reset.");
            }

            return Json(showMessageString, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> SecureReset(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                APIRequest req = new APIRequest
                {
                    RequestType = "ResetUser",
                    RequestString = id,
                    UserName = User.Identity.Name,
                    Message = string.Empty,
                };

                var resp = await WebAPIHelper.APIRequestAsync("/Auth/GetResetJobRequest/", req);
                if (resp != null && resp.Succeeded)
                {
                    ResetPasswordViewModel model = new ResetPasswordViewModel
                    {
                        RowId = resp.Code,
                        NewSecureCode = string.Empty,
                        ConfirmSecureCode = string.Empty,
                        CaptchaImage = SetupCaptchaBase64Image(),
                    };

                    return View(model);
                }
                else
                {
                    if (resp == null)
                    {
                        ViewBag.Error = "Invalid Request";
                    }
                    else if (!string.IsNullOrEmpty(resp.Message))
                    {
                        ViewBag.Error = resp.Message;
                    }
                    else
                    {
                        ViewBag.Error = "Request not found.";
                    }
                }
            }
            else
            {
                ViewBag.Error = "Error";
            }

            return View("InvalidRequest");
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ChangeSuccess()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ResetSecureCode(ResetPasswordViewModel model)
        {
            dynamic showMessageString = string.Empty;
            try
            {
                // check for Captcha
                if (Session["captchasessionvariable"] == null || string.IsNullOrEmpty(model.Captcha) ||
                   Session["captchasessionvariable"].ToString().ToUpper() != model.Captcha.ToUpper())
                {
                    string cap = Session["captchasessionvariable"].ToString();

                    LoggerHelper.Instance.TraceLog("RequestAccountReset - Invalid CaptCha");
                    showMessageString = new
                    {
                        Code = 203,
                        Message = "Invalid Captcha. Please refresh and re-enter Captcha code.",
                    };

                    Session["captchasessionvariable"] = "";
                    return Json(showMessageString, JsonRequestBehavior.AllowGet);
                }

                // not wrong then carry on
                string errmsg = string.Empty;
                if (ModelState.IsValid && model != null)
                {
                    Session["captchasessionvariable"] = "";
                    APIRequest req = new APIRequest
                    {
                        RequestType = "ResetSecureCode",
                        RequestString = model.RowId.ToString(),
                        Message = model.NewSecureCode,

                    };

                    var resp = await WebAPIHelper.APIRequestAsync("/Auth/ResetSecureCode/", req);
                    if (resp != null && resp.Succeeded)
                    {
                        showMessageString = new
                        {
                            Code = 200,
                            Message = "Success",
                        };
                    }
                    else
                    {
                        string msg = "Failed to reset password ";
                        if (resp != null && !string.IsNullOrEmpty(resp.Message))
                        {
                            msg += "- " + resp.Message;
                        }

                        showMessageString = new
                        {
                            Code = 202,
                            Message = msg,
                        };
                    }
                }
                else
                {
                    // validate by Model state
                    foreach (string key in ViewData.ModelState.Keys)
                    {
                        string modelStateError = string.Empty;
                        var modelState = ViewData.ModelState[key];
                        if (modelState.Errors != null && modelState.Errors.Count > 0)
                        {
                            string tmp = key.Replace("SecureCode", "Password");
                            errmsg = string.Format(@"{0}<br />{1}:{2}", errmsg, tmp, modelState.Errors.FirstOrDefault().ErrorMessage);
                        }
                    }

                    showMessageString = new
                    {
                        Code = 201, // invalid format
                        Message = !string.IsNullOrEmpty(errmsg) ? errmsg.Substring(6) : errmsg,
                        ModalType = "Add",
                    };
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Controller: Auth, Action: ResetSecureCode, Message: {0} ", ex.Message));
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Error in processing new Registration.");
            }

            return Json(showMessageString, JsonRequestBehavior.AllowGet);
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            LoggerHelper.Instance.TraceLog(model.Username + " Request to login  " );
            var result = await Helper.WebAPIHelper.UserAuthAsync("/Auth/Authentication/", model);

            if (result != null && result.IsAuth && result.AuthResult)
            {
                SetSession(result);

     

                return RedirectToLocal(returnUrl);

            }
            else
            {
                LoggerHelper.Instance.TraceLog(model.Username + "Fail to login  ");
                ModelState.AddModelError("", "Invalid login attempt.");
                return View(model);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Logoff()
        {
            try
            {
                if (!string.IsNullOrEmpty(User.Identity.Name))
                {
                    FormsAuthentication.SignOut();
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Module: AuthController, Action: none, Message: {0} ", ex.Message));
            }

            return RedirectToAction("", "");
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Index", "Home");
        }


        private void SetSession(AuthResponse result)
        {
            string encryptedTicket = string.Empty;
            double cookieMin = 30;
            if (!double.TryParse(ConfigurationManager.AppSettings["CookieMin"], out cookieMin))
            {
                cookieMin = 30;
            }

            if (FormsAuthentication.IsEnabled)
            {
                FormsAuthentication.SetAuthCookie(result.UserId, false);

            }

            var authTicket = new FormsAuthenticationTicket(1, result.UserId, DateTime.Now,
                DateTime.Now.AddMinutes(cookieMin), false, JsonConvert.SerializeObject(result), "/Auth/Login");

            encryptedTicket = FormsAuthentication.Encrypt(authTicket);

            var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
            HttpContext.Response.Cookies.Add(authCookie);


            Session["Department"] = "";
            if (result.DepartmentList != null && result.DepartmentList.Count > 0)
            {
                string idList = string.Empty;
                foreach (var item in result.DepartmentList)
                {

                    idList = string.Format("{0},{1}", idList, item.Department_ID);

                }

                if (!string.IsNullOrEmpty(idList))
                {
                    Session["DEPARTMENT"] = idList.Substring(1);
                }
            }

            if (result.ModuleList != null && result.ModuleList.Count > 0)
            {
                string idList = string.Empty;
                foreach (var item in result.ModuleList)
                {
                    idList = string.Format("{0},{1}", idList, item.ModuleName);
                }

                if (!string.IsNullOrEmpty(idList))
                {
                    Session["MODULE"] = idList.Substring(1);
                }
            }

            Session["USERKEY"] = result.UserId;
        }

        private string SetupCaptchaBase64Image()
        {
            string result = string.Empty;

            try
            {
                char[] charArr = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
                Random rand = new Random((int)DateTime.Now.Ticks);

                var captcha = string.Format("{0}{1}{2}{3}", charArr[rand.Next(0, 25)], charArr[rand.Next(0, 25)], charArr[rand.Next(0, 25)], charArr[rand.Next(0, 25)]);

                //store answer 
                Session["captchasessionvariable"] = captcha;

                using (var mem = new MemoryStream())
                using (var bmp = new Bitmap(100, 30))
                using (var gfx = Graphics.FromImage((Image)bmp))
                {

                    gfx.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
                    gfx.SmoothingMode = SmoothingMode.AntiAlias;
                    gfx.FillRectangle(Brushes.DarkBlue, new Rectangle(0, 0, bmp.Width, bmp.Height));

                    //add noise 
                    int i, r, x, y;
                    var pen = new Pen(Color.Yellow);
                    for (i = 1; i < 15; i++)
                    {
                        pen.Color = Color.FromArgb(
                        (rand.Next(0, 255)),
                        (rand.Next(0, 255)),
                        (rand.Next(0, 255)));

                        r = rand.Next(0, (130 / 3));
                        x = rand.Next(0, 130);
                        y = rand.Next(0, 30);

                        int posx = x - r;
                        int posy = y - r;
                        gfx.DrawEllipse(pen, posx, posy, r, r);
                    }


                    //add question 
                    gfx.DrawString(captcha, new Font("Tahoma", 15), Brushes.White, 20, 3);

                    //render as Jpeg 
                    bmp.Save(mem, System.Drawing.Imaging.ImageFormat.Jpeg);
                    result = string.Format("data:image/Jpeg;base64,{0}", Convert.ToBase64String(mem.GetBuffer()));

                    gfx.Dispose();
                    bmp.Dispose();
                    mem.Dispose();
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(ex.Message);
            }

            return result;
        }
    }
}