﻿/* Copyright © ATT Systems Pte Ltd. All Rights Reserved
 * Project      : ATTSystems.ProjectCore
 * Author       : Mark Tan
 * Created Date : 2019-06-07
 * Updated Date : 
 */

namespace ATTSystems.ProjectCore.Controllers
{
    using ATTSystems.Core.Logger;
    using ATTSystems.Core.Model.HttpModel;
    using ATTSystems.Core.Model.ViewModel;
    using ATTSystems.ProjectCore.Helper;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Mvc;

    [LocalAuthorize]
    public class SettingController : Controller
    {
        [HttpGet]
        public async Task<ActionResult> ListDepartment()
        {
            APIRequest req = new APIRequest
            {
                RequestType = "ListDepartment",
                RequestString = User.Identity.Name,
                UserName = User.Identity.Name,
                Message = string.Empty
            };

            List<DepartmentViewModel> result = null;
            //APIResponse
            var resp = await WebAPIHelper.APIRequestAsync("/Setting/GetDepartmentList/", req);
            if (resp != null && resp.DepartmentList != null)
            {
                result = resp.DepartmentList;
            }
            else
            {
                result = new List<DepartmentViewModel>();
            }

            return View(result);
        }

        [HttpGet]
        public ActionResult CreateDepartmentModal()
        {
            return PartialView("_AddDepartment", new DepartmentViewModel());
        }

        [HttpGet]
        public async Task<ActionResult> EditDepartmentModal(int id)
        {
            APIRequest req = new APIRequest
            {
                UserName = User.Identity.Name,
                Message = "Get department by id",
                RequestString = id.ToString(),
                RequestType = "Get single department",
            };

            var resp = await WebAPIHelper.APIRequestAsync("/Setting/GetDepartment/", req);

            if (resp != null && resp.Succeeded && resp.DepartmentList != null && resp.DepartmentList.Count > 0)
            {
                DepartmentViewModel model = resp.DepartmentList.FirstOrDefault();
                return PartialView("_EditDepartment", model);
            }
            else
            {
                string errmsg = (resp != null && !string.IsNullOrEmpty(resp.Message)) ? resp.Message : "Invalid input";

                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest, errmsg);
            }


        }

        [HttpPost]
        public async Task<ActionResult> DeleteDepartment(IdCollection idColl)
        {
            dynamic showMessageString = string.Empty;

            try
            {
                APIRequest req = new APIRequest
                {
                    UserName = User.Identity.Name,
                    Message = "Delete Department",
                    RequestString = idColl.IdList,
                    RequestType = "Delete",
                };

                var resp = await WebAPIHelper.APIRequestAsync("/Setting/DeleteDepartment/", req);
                if (resp != null && resp.Succeeded)
                {
                    showMessageString = new
                    {
                        Code = 200,
                        Message = string.Format("Department is sucessfully deleted"),
                        ModalType = "Delete",
                    };
                }
                else
                {
                    showMessageString = new
                    {
                        Code = 300,
                        Message = "Failed! Department setting is not deleted.",
                        ModalType = "Delete",
                    };
                }

                return Json(showMessageString, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Type: WebApp, Controller: Setting, Action: DeleteDepartment, Message: {0} ", ex.Message));

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Error in processing new settings.");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditDepartment(DepartmentViewModel model)
        {
            dynamic showMessageString = string.Empty;
            try
            {
                if (ModelState.IsValid && model != null)
                {
                    APIRequest request = new APIRequest
                    {
                        Model = model,
                        UserName = User.Identity.Name,
                    };

                    APIResponse resp = await WebAPIHelper.APIRequestAsync("/Setting/UpdateDepartment/", request);
                    if (resp == null || !resp.Succeeded)
                    {
                        string err = resp != null ? resp.Message : string.Empty;
                        showMessageString = new
                        {
                            Code = 300,
                            Message = "Failed. Department is not Updated.\n" + err,
                            ModalType = "Update",
                        };
                    }
                    else
                    {
                        showMessageString = new
                        {
                            Code = 200,
                            Message = string.Format("Department is sucessfully updated [id: {0}].", resp.Code),
                            ModalType = "Update",
                            Department_Name = model.Department_Name,
                            Description = model.Description,
                            Department_ID = model.Department_ID,
                        };
                    }
                }
                else
                {
                    string errmsg = string.Empty;
                    foreach (string key in ViewData.ModelState.Keys)
                    {
                        string modelStateError = string.Empty;
                        var modelState = ViewData.ModelState[key];
                        if (modelState.Errors != null && modelState.Errors.Count > 0)
                        {
                            errmsg = string.Format("{0}|{1}:{2}", errmsg, key, modelState.Errors.FirstOrDefault().ErrorMessage);
                        }
                    }

                    showMessageString = new
                    {
                        Code = 201, // invalid format
                        Message = !string.IsNullOrEmpty(errmsg) ? errmsg.Substring(1) : errmsg,
                        ModalType = "Update",
                    };
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Type: WebApp, Controller: Setting, Action: EditDepartment, Message: {0} ", ex.Message));
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Error in processing new Department.");
            }

            return Json(showMessageString, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateDepartment(DepartmentViewModel model)
        {
            dynamic showMessageString = string.Empty;

            try
            {
                if (ModelState.IsValid && model != null)
                {
                    APIRequest request = new APIRequest
                    {
                        Model = model,
                        UserName = User.Identity.Name,
                    };

                    APIResponse resp = await WebAPIHelper.APIRequestAsync("/Setting/AddDepartment/", request);
                    if (resp == null || !resp.Succeeded)
                    {
                        string err = resp != null ? resp.Message : string.Empty;
                        showMessageString = new
                        {
                            Code = 300,
                            Message = "Failed. Department is not created.\n" + err,
                            ModalType = "Add",
                        };
                    }
                    else
                    {
                        showMessageString = new
                        {
                            Code = 200,
                            Message = string.Format("Department is sucessfully created [id: {0}].", resp.Code),
                            ModalType = "Add",
                        };
                    }
                }
                else
                {
                    string errmsg = string.Empty;
                    foreach (string key in ViewData.ModelState.Keys)
                    {
                        string modelStateError = string.Empty;
                        var modelState = ViewData.ModelState[key];
                        if (modelState.Errors != null && modelState.Errors.Count > 0)
                        {
                            errmsg = string.Format("{0}|{1}:{2}", errmsg, key, modelState.Errors.FirstOrDefault().ErrorMessage);
                        }
                    }

                    showMessageString = new
                    {
                        Code = 201, // invalid format
                        Message = !string.IsNullOrEmpty(errmsg) ? errmsg.Substring(1) : errmsg,
                        ModalType = "Add",
                    };
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Type: WebApp, Controller: Setting, Action: CreateDepartment, Message: {0} ", ex.Message));
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Error in processing new Department.");
            }

            return Json(showMessageString, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteUser(IdCollection idColl)
        {
            dynamic showMessageString = string.Empty;

            try
            {
                APIRequest req = new APIRequest
                {
                    UserName = User.Identity.Name,
                    Message = "Delete User",
                    RequestString = idColl.IdList,
                    RequestType = "Delete",
                };

                var resp = await WebAPIHelper.APIRequestAsync("/Setting/DeleteUser/", req);
                if (resp != null && resp.Succeeded)
                {
                    showMessageString = new
                    {
                        Code = 200,
                        Message = string.Format("User is sucessfully deleted"),
                        ModalType = "Delete",
                    };
                }
                else
                {
                    showMessageString = new
                    {
                        Code = 300,
                        Message = "Failed! User is not updated.",
                        ModalType = "Delete",
                    };
                }

                return Json(showMessageString, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Type: WebApp, Controller: Setting, Action: DeleteUser, Message: {0} ", ex.Message));

                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Error in processing new settings.");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditUserAdmin(UserViewModel model, List<string> multirole, List<string> multidept)
        {
            dynamic showMessageString = string.Empty;
            try
            {
                if (ModelState.IsValid && model != null)
                {
                    if (multirole != null && multirole.Count > 0)
                    {
                        if (multidept != null && multidept.Count > 0)
                        {
                            model.RoleList = new List<RoleViewModel>();
                            foreach (string rId in multirole)
                            {
                                int roleId = 0;
                                if (int.TryParse(rId, out roleId))
                                {
                                    model.RoleList.Add(new RoleViewModel { Id = roleId, });
                                }
                            }

                            model.DepartmentList = new List<DepartmentViewModel>();
                            foreach (string deptId in multidept)
                            {
                                model.DepartmentList.Add(new DepartmentViewModel { Department_ID = int.Parse(deptId) });
                            }

                            APIRequest request = new APIRequest
                            {
                                Model = model,
                                UserName = User.Identity.Name,
                            };

                            APIResponse resp = await WebAPIHelper.APIRequestAsync("/Setting/UpdateUser/", request);
                            if (resp == null || !resp.Succeeded)
                            {
                                string err = resp != null ? resp.Message : string.Empty;
                                showMessageString = new
                                {
                                    Code = 300,
                                    Message = "Failed. User is not updated.\n" + err,
                                    ModalType = "Update",
                                };
                            }
                            else
                            {
                                showMessageString = new
                                {
                                    Code = 200,
                                    Message = string.Format("User is sucessfully updated."),
                                    ModalType = "Update",
                                };
                            }
                        }
                    }
                }
                else
                {
                    string errmsg = string.Empty;
                    foreach (string key in ViewData.ModelState.Keys)
                    {
                        string modelStateError = string.Empty;
                        var modelState = ViewData.ModelState[key];
                        if (modelState.Errors != null && modelState.Errors.Count > 0)
                        {
                            errmsg = string.Format("{0}|{1}:{2}", errmsg, key, modelState.Errors.FirstOrDefault().ErrorMessage);
                        }
                    }

                    showMessageString = new
                    {
                        Code = 201, // invalid format
                        Message = !string.IsNullOrEmpty(errmsg) ? errmsg.Substring(1) : errmsg,
                        ModalType = "Update",
                    };
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Type: WebApp, Controller: Setting, Action: EditUserAdmin, Message: {0} ", ex.Message));
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Error in processing new Role.");
            }

            return Json(showMessageString, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateUserAdmin(UserViewModel model, List<string> multirole, List<string> multidept)
        {
            dynamic showMessageString = string.Empty;

            string errMsg = string.Empty;
            try
            {
                if (ModelState.IsValid && model != null)
                {
                    if (multirole != null && multirole.Count > 0)
                    {
                        if (multidept != null && multidept.Count > 0)
                        {
                            model.RoleList = new List<RoleViewModel>();
                            foreach (string rId in multirole)
                            {
                                int roleId = 0;
                                if (int.TryParse(rId, out roleId))
                                {
                                    model.RoleList.Add(new RoleViewModel { Id = roleId, });
                                }
                            }

                            model.DepartmentList = new List<DepartmentViewModel>();
                            foreach (string deptId in multidept)
                            {
                                model.DepartmentList.Add(new DepartmentViewModel { Department_ID = int.Parse(deptId) });
                            }

                            APIRequest request = new APIRequest
                            {
                                Model = model,
                                UserName = User.Identity.Name,
                            };

                            APIResponse resp = await WebAPIHelper.APIRequestAsync("/Setting/AddUser/", request);
                            if (resp == null || !resp.Succeeded)
                            {
                                string err = resp != null ? resp.Message : string.Empty;
                                showMessageString = new
                                {
                                    Code = 300,
                                    Message = "Failed. User is not created.\n" + err,
                                    ModalType = "Add",
                                };
                            }
                            else
                            {
                                showMessageString = new
                                {
                                    Code = 200,
                                    Message = string.Format("User is sucessfully created [id: {0}].", resp.Code),
                                    ModalType = "Add",
                                };
                            }
                        }
                        else
                        {
                            errMsg = "System did not find any Department being selected.";
                        }
                    }
                    else
                    {
                        errMsg = "System did not find any Role being selected.";
                    }

                    if (!string.IsNullOrEmpty(errMsg))
                    {
                        showMessageString = new
                        {
                            Code = 202,
                            Message = errMsg,
                            ModalType = "Add",
                        };
                    }
                }
                else
                {
                    string errmsg = string.Empty;
                    foreach (string key in ViewData.ModelState.Keys)
                    {
                        string modelStateError = string.Empty;
                        var modelState = ViewData.ModelState[key];
                        if (modelState.Errors != null && modelState.Errors.Count > 0)
                        {
                            errmsg = string.Format("{0}|{1}:{2}", errmsg, key, modelState.Errors.FirstOrDefault().ErrorMessage);
                        }
                    }

                    showMessageString = new
                    {
                        Code = 201, // invalid format
                        Message = !string.IsNullOrEmpty(errmsg) ? errmsg.Substring(1) : errmsg,
                        ModalType = "Add",
                    };
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Type: WebApp, Controller: Setting, Action: CreateUserAdmin, Message: {0} ", ex.Message));
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Error in processing new Department.");
            }

            return Json(showMessageString, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<ActionResult> ListUser()
        {
            APIRequest req = new APIRequest
            {
                RequestType = "ListUser",
                RequestString = User.Identity.Name,
                UserName = User.Identity.Name,
                Message = string.Empty
            };

            List<UserViewModel> result = null;
            var resp = await WebAPIHelper.APIRequestAsync("/Setting/GetUserList/", req);

            if (resp != null && resp.UserList != null)
            {
                result = resp.UserList;
            }
            else
            {
                result = new List<UserViewModel>();
            }

            return View(result);
        }

        [HttpGet]
        public async Task<ActionResult> EditUserModal(string id)
        {
            UserViewModel result = null;
            // get Role list and Department List
            APIRequest req = new APIRequest
            {
                RequestType = "GetSingleUser",
                RequestString = id,
                UserName = string.Empty,
                Message = string.Empty
            };

            var resp = await WebAPIHelper.APIRequestAsync("/Setting/GetUser/", req);

            if (resp != null && resp.Succeeded && resp.UserList != null && resp.UserList.Count > 0)
            {
                result = resp.UserList.FirstOrDefault();

                if (result != null)
                {
                    result.ConfirmPassword = "111111";
                    result.Password = "111111";
                }
                else
                {
                    string errmsg = (resp != null && !string.IsNullOrEmpty(resp.Message)) ? resp.Message : "Invalid input";
                    return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest, errmsg);
                }
            }
            else
            {
                string errmsg = (resp != null && !string.IsNullOrEmpty(resp.Message)) ? resp.Message : "Invalid input";

                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest, errmsg);
            }

            return PartialView("_EditUser", result);
            //return PartialView("_AddUser", result);
        }

        [HttpGet]
        public async Task<ActionResult> CreateUserModal()
        {
            // get Role list and Department List
            APIRequest req = new APIRequest
            {
                RequestType = "GetRoleDepartmentList",
                RequestString = User.Identity.Name,
                UserName = string.Empty,
                Message = string.Empty
            };

            UserViewModel result = null;

            //APIResponse
            var resp = await WebAPIHelper.APIRequestAsync("/Setting/GetRoleDepartmentList/", req);

            if (resp != null)
            {
                if (resp.RoleList != null)
                {
                    if (resp.DepartmentList != null)
                    {
                        result = new UserViewModel();
                        result.DepartmentList = resp.DepartmentList;
                        result.RoleList = resp.RoleList;
                    }
                    else
                    {
                        return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest, "Department list not available");
                    }
                }
                else
                {
                    return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest, "Role list not available");
                }
            }

            return PartialView("_AddUser", result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateRoleAdmin(RoleViewModel model, params string[] selectedRoles)
        {
            dynamic showMessageString = string.Empty;

            try
            {
                if (ModelState.IsValid && model != null)
                {

                    if (selectedRoles != null && selectedRoles.Length > 0)
                    {
                        model.ModuleList = new List<ModuleViewModel>();

                        foreach (string item in selectedRoles)
                        {
                            model.ModuleList.Add(new ModuleViewModel { Id = int.Parse(item) });
                        }

                        APIRequest request = new APIRequest
                        {
                            Model = model,
                            UserName = User.Identity.Name,
                        };

                        APIResponse resp = await WebAPIHelper.APIRequestAsync("/Setting/AddRole/", request);
                        if (resp == null || !resp.Succeeded)
                        {
                            string err = resp != null ? resp.Message : string.Empty;
                            showMessageString = new
                            {
                                Code = 300,
                                Message = "Failed. Role is not created.\n" + err,
                                ModalType = "Add",
                            };
                        }
                        else
                        {
                            showMessageString = new
                            {
                                Code = 200,
                                Message = string.Format("Role is sucessfully created [id: {0}].", resp.Code),
                                ModalType = "Add",
                            };
                        }
                    }
                    else
                    {
                        showMessageString = new
                        {
                            Code = 202,
                            Message = "System did not found and module selected. ",
                            ModalType = "Add",
                        };
                    }
                }
                else
                {
                    string errmsg = string.Empty;
                    foreach (string key in ViewData.ModelState.Keys)
                    {
                        string modelStateError = string.Empty;
                        var modelState = ViewData.ModelState[key];
                        if (modelState.Errors != null && modelState.Errors.Count > 0)
                        {
                            errmsg = string.Format("{0}|{1}:{2}", errmsg, key, modelState.Errors.FirstOrDefault().ErrorMessage);
                        }
                    }

                    showMessageString = new
                    {
                        Code = 201, // invalid format
                        Message = !string.IsNullOrEmpty(errmsg) ? errmsg.Substring(1) : errmsg,
                        ModalType = "Add",
                    };
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Type: WebApp, Controller: Setting, Action: CreateRoleAdmin, Message: {0} ", ex.Message));
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Error in processing new Role.");
            }

            return Json(showMessageString, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<ActionResult> ListRole()
        {
            APIRequest req = new APIRequest
            {
                RequestType = "ListRole",
                RequestString = User.Identity.Name,
                UserName = User.Identity.Name,
                Message = string.Empty
            };

            List<RoleViewModel> result = null;
            var resp = await WebAPIHelper.APIRequestAsync("/Setting/GetRoleList/", req);
            if (resp != null && resp.RoleList != null)
            {
                result = resp.RoleList;
            }
            else
            {
                result = new List<RoleViewModel>();
            }

            return View(result);
        }

        [HttpGet]
        public async Task<ActionResult> CreateRoleModal()
        {
            APIRequest req = new APIRequest
            {
                RequestType = "GetModuleList",
                RequestString = User.Identity.Name,
                UserName = string.Empty,
                Message = string.Empty
            };

            RoleViewModel result = null;
            //APIResponse
            var resp = await WebAPIHelper.APIRequestAsync("/Setting/GetModuleList/", req);

            if (resp != null && resp.ModuleList != null)
            {
                result = new RoleViewModel();
                result.ModuleList = resp.ModuleList;
            }
            else
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest, "Module not available");
            }

            return PartialView("_AddRole", result);
            //return View(result);

        }

        [HttpGet]
        public async Task<ActionResult> EditRoleModal(string id)
        {
            APIRequest req = new APIRequest
            {
                UserName = User.Identity.Name,
                Message = "Get role for editing by id",
                RequestString = id,
                RequestType = "Get single role for editing",
            };

            var resp = await WebAPIHelper.APIRequestAsync("/Setting/GetRole/", req);

            if (resp != null && resp.Succeeded && resp.RoleList != null && resp.RoleList.Count > 0)
            {
                RoleViewModel model = resp.RoleList.FirstOrDefault();
                return PartialView("_EditRole", model);
            }
            else
            {
                string errmsg = (resp != null && !string.IsNullOrEmpty(resp.Message)) ? resp.Message : "Invalid input";

                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest, errmsg);
            }
        }

        [HttpPost]
        public async Task<ActionResult> DeleteRole(IdCollection idColl)
        {
            dynamic showMessageString = string.Empty;

            try
            {
                APIRequest req = new APIRequest
                {
                    UserName = User.Identity.Name,
                    Message = "Delete Role",
                    RequestString = idColl.IdList,
                    RequestType = "Delete",
                };

                var resp = await WebAPIHelper.APIRequestAsync("/Setting/DeleteRole/", req);
                if (resp != null && resp.Succeeded)
                {
                    showMessageString = new
                    {
                        Code = 200,
                        Message = string.Format("Role is sucessfully deleted"),
                        ModalType = "Delete",
                    };
                }
                else
                {
                    showMessageString = new
                    {
                        Code = 300,
                        Message = "Failed! Role setting is not updated.",
                        ModalType = "Delete",
                    };
                }

                return Json(showMessageString, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Type: WebApp, Controller: Setting, Action: DeleteRole, Message: {0} ", ex.Message));
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Error in processing new settings.");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditRoleAdmin(RoleViewModel model, params string[] selectedRoles)
        {
            dynamic showMessageString = string.Empty;

            try
            {
                if (ModelState.IsValid && model != null)
                {
                    if (selectedRoles != null && selectedRoles.Length > 0)
                    {
                        model.ModuleList = new List<ModuleViewModel>();

                        foreach (string item in selectedRoles)
                        {
                            model.ModuleList.Add(new ModuleViewModel { Id = int.Parse(item) });
                        }

                        APIRequest request = new APIRequest
                        {
                            UserName = User.Identity.Name,
                            Model = model,
                        };

                        APIResponse resp = await WebAPIHelper.APIRequestAsync("/Setting/UpdateRole/", request);
                        if (resp == null || !resp.Succeeded)
                        {
                            string err = resp != null ? resp.Message : string.Empty;
                            showMessageString = new
                            {
                                Code = 300,
                                Message = "Failed. Role is not updated.\n" + err,
                                ModalType = "Update",
                            };
                        }
                        else
                        {
                            showMessageString = new
                            {
                                Code = 200,
                                Message = string.Format("Role is sucessfully updated."),
                                ModalType = "Update",
                            };
                        }
                    }
                    else
                    {
                        showMessageString = new
                        {
                            Code = 202,
                            Message = "System did not found and module selected. ",
                            ModalType = "Update",
                        };
                    }
                }
                else
                {
                    string errmsg = string.Empty;
                    foreach (string key in ViewData.ModelState.Keys)
                    {
                        string modelStateError = string.Empty;
                        var modelState = ViewData.ModelState[key];
                        if (modelState.Errors != null && modelState.Errors.Count > 0)
                        {
                            errmsg = string.Format("{0}|{1}:{2}", errmsg, key, modelState.Errors.FirstOrDefault().ErrorMessage);
                        }
                    }

                    showMessageString = new
                    {
                        Code = 201, // invalid format
                        Message = !string.IsNullOrEmpty(errmsg) ? errmsg.Substring(1) : errmsg,
                        ModalType = "Update",
                    };
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Type: WebApp, Controller: Setting, Action: EditRoleAdmin, Message: {0} ", ex.Message));
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Error in modifying Role.");
            }

            return Json(showMessageString, JsonRequestBehavior.AllowGet);
        }
    }
}