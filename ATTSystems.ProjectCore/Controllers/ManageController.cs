﻿/* Copyright © ATT Systems Pte Ltd. All Rights Reserved
 * Project      : ATTSystems.ProjectCore
 * Author       : Mark Tan
 * Created Date : 2019-06-07
 * Updated Date : 
 */

namespace ATTSystems.ProjectCore.Controllers
{
    using ATTSystems.Core.Logger;
    using ATTSystems.Core.Model.ViewModel;
    using ATTSystems.ProjectCore.Helper;
    using System;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Mvc;

    [LocalAuthorize]
    public class ManageController : Controller
    {
        public ActionResult RequestChangeKeycode()
        {
            ChangePasswordViewModel model = new ChangePasswordViewModel
            {
                UserName = User.Identity.Name,
            };

            return View(model);
        }

        //
        // GET: /Manage/ChangePassword
        //public ActionResult ChangePassword(string userid)
        //{
        //    ChangePasswordViewModel model = new ChangePasswordViewModel();
        //   // if (string.IsNullOrEmpty(userid))// then used current userid
        //    //{
        //        //model = new ChangePasswordViewModel();
        //        model.UserName = User.Identity.Name;
        //    //}

        //    return View(model);
        //}

        //
        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            dynamic showMessageString = string.Empty;
            try
            {
                string errmsg = string.Empty;
                if (ModelState.IsValid && model != null)
                {
                    if (model.NewPassword == model.ConfirmPassword)
                    {
                        model.UserName = User.Identity.Name;
                        var result = await Helper.WebAPIHelper.ChangePasswordAsync("/Auth/ChangePassword/", model);

                        if (result != null && result.Succeeded)
                        {
                            //return RedirectToAction("Login", "Account");
                            ViewBag.StatusMessage = "Password Change have been successfully";
                            showMessageString = new
                            {
                                Code = 200,
                                Message = "Success.",
                            };
                        }
                        else
                        {
                            string msg = result != null ? result.Message : "Internal error.";
                            ModelState.AddModelError("", result.Message);
                        }
                    }
                    else
                    {
                        showMessageString = new
                        {
                            Code = 202,
                            Message = "New and Confirmed Password are different.",
                        };
                    }
                }
                else
                {
                    // validate by Model state
                    foreach (string key in ViewData.ModelState.Keys)
                    {
                        string modelStateError = string.Empty;
                        var modelState = ViewData.ModelState[key];
                        if (modelState.Errors != null && modelState.Errors.Count > 0)
                        {
                            string tmp = key.Replace("SecureCode", "Password");
                            errmsg = string.Format(@"{0}<br />{1}:{2}", errmsg, tmp, modelState.Errors.FirstOrDefault().ErrorMessage);
                        }
                    }

                    showMessageString = new
                    {
                        Code = 201, // invalid format
                        Message = !string.IsNullOrEmpty(errmsg) ? errmsg.Substring(6) : errmsg,
                        ModalType = "Add",
                    };
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Controller: Manage, Action: ChangePassword, Message: {0} ", ex.Message));
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError, "Error in processing.");
            }

            return Json(showMessageString, JsonRequestBehavior.AllowGet);
        }
    }
}