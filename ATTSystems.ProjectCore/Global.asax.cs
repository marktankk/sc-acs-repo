﻿/* Copyright © ATT Systems Pte Ltd. All Rights Reserved
 * Project      : ATTSystems.ProjectCore
 * Author       : Mark Tan
 * Created Date : 2019-06-07
 * Updated Date : 
 */
namespace ATTSystems.ProjectCore
{
    using System;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;
    using ATTSystems.Core;
    using ATTSystems.Core.Logger;

    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            APPReference.Registered();
            
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            try
            {
                Exception exception = Server.GetLastError();
                LoggerHelper.Instance.LogError(exception);

                Response.Redirect("/Home/Error");
            }
            catch { }
        }
    }
}
