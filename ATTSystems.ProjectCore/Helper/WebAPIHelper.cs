﻿/* Copyright © ATT Systems Pte Ltd. All Rights Reserved
 * --------------------------------------------------------------------------
 * 
 * Project      : ATTSystems.ProjectCore
 * Author       : Mark Tan
 * Created Date : 2019-06-07
 * 
 * 
 */

namespace ATTSystems.ProjectCore.Helper
{
    using ATTSystems.Core.Logger;
    using ATTSystems.Core.Model.HttpModel;
    using ATTSystems.Core.Model.ViewModel;
    using ATTSystems.Core.Utilities;
    using ATTSystems.ProjectCore.Model.HttpModel;
    using Newtonsoft.Json;
    using System;
    using System.Configuration;
    using System.Threading.Tasks;

    public class WebAPIHelper
    {
        public static async Task<APIResponse> APIRequestAsync(string relativeUri, APIRequest request)
        {
            APIResponse result = null;

            try
            {
                WebAPIClient webApiClient = new WebAPIClient();
                var response = await webApiClient.PostAsync(ConfigurationManager.AppSettings["APIBaseUri"], relativeUri, JsonConvert.SerializeObject(request));

                if ((response == null) || (response.IsSuccessStatusCode == false))
                {
                    return null;
                }

                result = JsonConvert.DeserializeObject<APIResponse>(response.Content.ReadAsStringAsync().Result);
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, ClassType: WebAPIHelper, Method: APIRequestAsync, Message: {0} ", ex.Message));

                result = null;
            }

            return result;
        }

        public static async Task<AuthResponse> UserAuthAsync(string relativeUri, LoginViewModel loginViewModel)
        {
            AuthResponse result = null;
            try
            {
                WebAPIClient webApiClient = new WebAPIClient();
                string uri = ConfigurationManager.AppSettings["APIBaseUri"];
                var response = await webApiClient.PostAsync(ConfigurationManager.AppSettings["APIBaseUri"], relativeUri, JsonConvert.SerializeObject(loginViewModel));

                if ((response == null) || (response.IsSuccessStatusCode == false))
                {
                    return null;
                }

                result = JsonConvert.DeserializeObject<AuthResponse>(response.Content.ReadAsStringAsync().Result);
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, ClassType: WebAPIHelper, Method: UserAuthAsync, Message: {0} ", ex.Message));
                result = null;
            }

            return result;

        }

        public static async Task<APIResponse> ChangePasswordAsync(string relativeUri, ChangePasswordViewModel model)
        {
            APIResponse result = null;

            try
            {
                WebAPIClient webApiClient = new WebAPIClient();
                var response = await webApiClient.PostAsync(ConfigurationManager.AppSettings["APIBaseUri"], relativeUri, JsonConvert.SerializeObject(model));

                if ((response == null) || (response.IsSuccessStatusCode == false))
                {
                    return null;
                }

                result = JsonConvert.DeserializeObject<APIResponse>(response.Content.ReadAsStringAsync().Result);

            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, ClassType: WebAPIHelper, Method: ChangePasswordAsync, Message: {0} ", ex.Message));
                result = null;
            }

            return result;
        }

        public static async Task<AppResponse> AppRequestAsync(string relativeUri, APIRequest request)
        {
            AppResponse result = null;

            try
            {
                WebAPIClient webApiClient = new WebAPIClient();
                var response = await webApiClient.PostAsync(ConfigurationManager.AppSettings["APIBaseUri"], relativeUri, JsonConvert.SerializeObject(request));

                if ((response == null) || (response.IsSuccessStatusCode == false))
                {
                    return null;
                }

                result = JsonConvert.DeserializeObject<AppResponse>(response.Content.ReadAsStringAsync().Result);
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, ClassType: WebAPIHelper, Method: APIRequestAsync - AppResponse, Message: {0} ", ex.Message));

                result = null;
            }

            return result;
        }
    }
}