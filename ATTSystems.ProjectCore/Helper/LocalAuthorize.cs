﻿
/* Copyright © ATT Systems Pte Ltd. All Rights Reserved
 * --------------------------------------------------------------------------
 * 
 * Project      : ATTSystems.ProjectCore
 * Author       : Mark Tan
 * Created Date : 2019-06-07
 * Updated Date : 2019-09-21
 * Include session check and redirect to failed page
 * 
 */

namespace ATTSystems.ProjectCore.Helper
{
    using System.Configuration;
    using System.Web;
    using System.Web.Mvc;

    public class LocalAuthorize : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name) || HttpContext.Current.Session["USERKEY"] == null)
            {
                var urlHelper = new UrlHelper(filterContext.RequestContext);
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    filterContext.HttpContext.Response.StatusCode = 304;
                    filterContext.Result = new JsonResult
                    {
                        Data = new
                        {
                            Error = "NotAuthorized",
                            LogOnUrl = urlHelper.Action("Expired", "Auth")
                        },
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                    string url = urlHelper.Action("Expired", "Auth");

                    filterContext.HttpContext.Response.Redirect(url);
                }
                else
                {
                    string url = urlHelper.Action("Expired", "Auth");

                    filterContext.HttpContext.Response.Redirect(url);
                }

            }
            else
            {
                base.OnAuthorization(filterContext);
            }

        }
    }
}