﻿var url = window.location.href;
var urlArr = url.split('/');
var urlPath = '';
var data = '';
for (i = 0; i < urlArr.length - 2; i++) {
    urlPath += urlArr[i] + "/";
}

function LogoutDialog(dataField) {

    swal({
        title: "Confirm to logout?",
        text: "",
        type: "question",
        showCancelButton: true,
        confirmButtonColor: "#dd6b55",
        confirmButtonText: "Yes, logout"
    }).then(
        function () {
            $('#' + dataField).submit();
        },
        function (dismiss) {
            if (dismiss === 'cancel') {
            }
        }
        );
}

function deleteDialog(controller, action, rowid) {
    var text = "You will not be able to recover this record!";
    deleteDialogText(text, controller, action, rowid);

}

function deleteDialogText(text, controller, action, rowid) {
    var dataObj = "";
    for (i = 0; i < rowid.length; i++) {
        dataObj += rowid[i] + ",";
    }

 
    var title = "Confirm to delete selected row?";
    var confirmButtonText = "Yes, delete it!";

    swal({
        title: title,
        text: text,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: confirmButtonText
    }).then(
        function () {
            //if (module.toUpperCase() == "MASTER") {
            //    $('#' + dataField).submit();
            //}
            //else {
            $.ajax({
                url: urlPath + controller + "/" + action,
                data: { IdList: dataObj.substring(0, dataObj.length - 1) }, //JSON.parse(dataObj),
                type: "POST",
                cache: false,
                beforeSend: function () {
                },
                complete: function () {
                },
                success: function (e) {
                    if (e.Code == 200) { // ok
                        $('#row' + rowid[0]).remove();
                        showDialog("Record has been deleted.", "Deleted!", "success", "", "", 0, false, true);
                    }
                    else {
                        showDialog(e.Message + " " + action, "Error!", "error", "", "", 0, false, true);
                    }



                },
                error: function (e) {
                    showDialog(e.Message + " " + action, "Error!", "error", "", "", 0, false, true);
                }
            });
            //}
        },
        function (dismiss) {
            if (dismiss === 'cancel') {
            }
        }
        );
}

function showDialog(text, title, type, controller, action, rowid, isupdate, isDeleted) {

    swal({
        title: title,
        text: text,
        type: type,
        timer: 3000,
        //showCancelButton: true,
       // cancelButtonText: "OK",
        cancelButtonColor: '#3085d6',
        showConfirmButton: false
    }).then(
        function () {
        },
        function (dismiss) {
            if (dismiss === 'timer' || dismiss === 'cancel') {
                if (type == "success" && isDeleted == false && isupdate == false) {
                    RefreshList(controller, action);
                }
            }
        }
        );
}

function RefreshList(controller, action) {
    var relativeURI = urlPath + controller + "/" + action;
    renderPartialView(relativeURI);
}