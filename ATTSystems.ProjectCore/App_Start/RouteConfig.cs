﻿
/* Copyright © ATT Systems Pte Ltd. All Rights Reserved
 * Project      : ATTSystems.ProjectCore
 * Author       : Mark Tan
 * Created Date : 2019-06-07
 * Updated Date : 
 */

namespace ATTSystems.ProjectCore
{
    using System.Web.Mvc;
    using System.Web.Routing;

    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Auth", action = "Login", id = UrlParameter.Optional }
            );
        }
    }
}
