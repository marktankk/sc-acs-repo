﻿/* Copyright © ATT Systems Pte Ltd. All Rights Reserved
 * Project      : ATTSystems.Project.Model
 * Author       : Mark Tan
 * Created Date : 2019-06-07
 * Updated Date : 
 */

using ATTSystems.ProjectCore.Model.ViewModel;

namespace ATTSystems.ProjectCore.Model.HttpModel
{
    public class AppResponse
    {
        public AppResponse()
        {
            Succeeded = false;
            Code = 0;
            Message = "none";
            ID = 0;
        }

        public bool Succeeded
        {
            get;
            set;
        }

        public int Code
        {
            get;
            set;
        }

        public string Message
        {
            get;
            set;
        }

        public int ID
        {
            get;
            set;
        }

        // Add your collection here for transporting data
        // Example: -
        public DashboardViewModel DashboardView { get; set; }

    }
}
