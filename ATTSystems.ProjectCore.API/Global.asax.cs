﻿/* Copyright © ATT Systems Pte Ltd. All Rights Reserved
 * Project      : ATTSystems.ProjectCore.API
 * Author       : Mark Tan
 * Created Date : 2019-06-07
 * Updated Date : 
 */
namespace ATTSystems.ProjectCore.API
{
    using ATTSystems.Core;
    using ATTSystems.Core.Logger;
    using System;
    using System.Web.Http;
    using System.Web.Mvc;
    using System.Web.Routing;

    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            APPReference.Registered();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            LoggerHelper.Instance.LogError(exception);
        }
    }
}
