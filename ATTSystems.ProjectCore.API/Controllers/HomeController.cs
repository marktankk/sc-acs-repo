﻿/* Copyright © ATT Systems Pte Ltd. All Rights Reserved
 * Project      : ATTSystems.ProjectCore.API
 * Author       : Mark Tan
 * Created Date : 2019-06-07
 * Updated Date : 
 */

namespace ATTSystems.ProjectCore.API.Controllers
{
    using System.Web.Mvc;
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
