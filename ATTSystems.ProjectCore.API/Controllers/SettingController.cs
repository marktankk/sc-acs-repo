﻿/* Copyright © ATT Systems Pte Ltd. All Rights Reserved
 * Project      : ATTSystems.ProjectCore.API
 * Author       : Mark Tan
 * Created Date : 2019-06-07
 * Updated Date : 
 */

namespace ATTSystems.ProjectCore.API.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web.Http;
    using Newtonsoft.Json;
    using ATTSystems.Core.Logger;
    using ATTSystems.Core.Model.DBModel;
    using ATTSystems.Core.Model.HttpModel;
    using ATTSystems.Core.Model.ViewModel;
    using ATTSystems.ProjectCore.DAL.Implementations;
    using ATTSystems.ProjectCore.DAL.Interfaces;

    [RoutePrefix("api/setting")]
    public class SettingController : ApiController
    {
        [HttpPost]
        [Route("GetModuleList")]
        public async Task<HttpResponseMessage> GetModuleList(APIRequest request)
        {
            APIResponse result = new APIResponse();
            var response = this.Request.CreateResponse(HttpStatusCode.OK);

            try
            {
                SettingRepository _repository = new SettingRepository();
                List<Module> moduleList = await _repository.GetModuleListAsync();

                if (moduleList != null && moduleList.Count > 0)
                {
                    result.ModuleList = new List<Core.Model.ViewModel.ModuleViewModel>();
                    foreach (Module module in moduleList)
                    {
                        result.ModuleList.Add(new Core.Model.ViewModel.ModuleViewModel
                        {
                            Id = module.Id,
                            ModuleId = module.ModuleId,
                            ModuleName = module.ModuleName,
                        });
                    }

                    result.Message = "Success.";
                }
                else
                {
                    result.Message = _repository.GetErrorMsg();
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Type: API, Controller: Setting, Action: GetModuleList, Message: {0} ", ex.Message));
                result.Message = ex.Message;
            }

            response.Content = new StringContent(JsonConvert.SerializeObject(result), System.Text.Encoding.UTF8, "application/json");

            return await Task.FromResult<HttpResponseMessage>(response);
        }

        [HttpPost]
        [Route("UpdateDepartment")]
        public async Task<HttpResponseMessage> UpdateDepartment(APIRequest request)
        {
            APIResponse result = new APIResponse();
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            try
            {
                SettingRepository _repository = new SettingRepository();
                int dbResult = await _repository.UpdateDepartmentAsync(request);
                if (dbResult > 0)
                {
                    result.Code = dbResult;
                    result.Succeeded = true;
                    result.Message = "Department sucessfully updated";
                }
                else
                {
                    result.Code = dbResult;
                    result.Succeeded = false;
                    result.Message = _repository.GetErrorMsg();
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Type: API, Controller: Setting, Action: UpdateDepartment, Message: {0} ", ex.Message));
                response = this.Request.CreateResponse(HttpStatusCode.InternalServerError);
            }

            response.Content = new StringContent(JsonConvert.SerializeObject(result), System.Text.Encoding.UTF8, "application/json");

            return await Task.FromResult<HttpResponseMessage>(response);
        }

        [HttpPost]
        [Route("AddDepartment")]
        public async Task<HttpResponseMessage> AddDepartment(APIRequest request)
        {
            APIResponse result = new APIResponse();
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            try
            {
                SettingRepository _repository = new SettingRepository();
                int dbResult = await _repository.AddDepartmentAsync(request);

                if (dbResult > 0)
                {
                    result.Code = dbResult;
                    result.Succeeded = true;
                    result.Message = "New Department sucessfully created";
                }
                else
                {
                    result.Code = dbResult;
                    result.Succeeded = false;
                    result.Message = _repository.GetErrorMsg();
                }

            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Type: API, Controller: Setting, Action: AddDepartment, Message: {0} ", ex.Message));
                response = this.Request.CreateResponse(HttpStatusCode.InternalServerError);
            }

            response.Content = new StringContent(JsonConvert.SerializeObject(result), System.Text.Encoding.UTF8, "application/json");

            return await Task.FromResult<HttpResponseMessage>(response);
        }

        [HttpPost]
        [Route("GetDepartmentList")]
        public async Task<HttpResponseMessage> GetDepartmentList(APIRequest request)
        {
            APIResponse result = new APIResponse();
            var response = this.Request.CreateResponse(HttpStatusCode.OK);

            try
            {
                SettingRepository _repository = new SettingRepository();
                List<Department> deptList = await _repository.GetDepartmentListAsync(request.UserName);

                if (deptList != null && deptList.Count > 0)
                {
                    result.DepartmentList = new List<Core.Model.ViewModel.DepartmentViewModel>();
                    foreach (Department dept in deptList)
                    {
                        result.DepartmentList.Add(new Core.Model.ViewModel.DepartmentViewModel
                        {
                            Department_ID = dept.Department_ID,
                            Department_Name = dept.Department_Name,
                            Description = dept.Description
                        });
                    }
                    result.Message = "Success.";
                }

            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Type: API, Controller: Setting, Action: GetDepartmentList, Message: {0} ", ex.Message));
                result.Message = ex.Message;
            }

            response.Content = new StringContent(JsonConvert.SerializeObject(result), System.Text.Encoding.UTF8, "application/json");

            return await Task.FromResult<HttpResponseMessage>(response);
        }

        [HttpPost]
        [Route("GetDepartment")]
        public async Task<HttpResponseMessage> GetDepartment(APIRequest request)
        {
            APIResponse result = new APIResponse();
            var response = this.Request.CreateResponse(HttpStatusCode.OK);

            try
            {
                int id = 0;
                if (int.TryParse(request.RequestString, out id))
                {
                    SettingRepository _repository = new SettingRepository();
                    Department dept = await _repository.GetDepartmentAsync(id);
                    if (dept != null)
                    {
                        DepartmentViewModel model = new DepartmentViewModel
                        {
                            Department_ID = dept.Department_ID,
                            Department_Name = dept.Department_Name,
                            Description = dept.Description
                        };
                        result.DepartmentList = new List<DepartmentViewModel>();
                        result.DepartmentList.Add(model); // use this to transport bacl to client
                        result.Message = "Department retrieval ok.";
                        result.Code = dept.Department_ID;
                        result.Succeeded = true;

                    }
                    else
                    {
                        result.Message = _repository.GetErrorMsg();
                        if (string.IsNullOrEmpty(result.Message))
                        {
                            result.Message = "System unable retrieve department.";
                        }
                    }
                }
                else
                {
                    result.Message = "Invalid Department Id";
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Type: API, Controller: Setting, Action: GetDepartment, Message: {0} ", ex.Message));
                result.Message = ex.Message;
            }

            response.Content = new StringContent(JsonConvert.SerializeObject(result), System.Text.Encoding.UTF8, "application/json");

            return await Task.FromResult<HttpResponseMessage>(response);
        }

        [HttpPost]
        [Route("DeleteDepartment")]
        public async Task<HttpResponseMessage> DeleteDepartment(APIRequest request)
        {
            APIResponse result = new APIResponse();
            var response = this.Request.CreateResponse(HttpStatusCode.OK);

            try
            {
                SettingRepository _repository = new SettingRepository();
                result.Code = await _repository.DeleteDepartmentAsync(request);

                if (result.Code > 0)
                {
                    result.Succeeded = true;
                    result.Message = "Department sucessfully deleted.";
                }
                else
                {
                    result.Succeeded = false;
                    result.Message = _repository.GetErrorMsg();
                }

            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Type: API, Controller: Setting, Action: DeleteDepartment, Message: {0} ", ex.Message));
                response = this.Request.CreateResponse(HttpStatusCode.InternalServerError);
            }

            response.Content = new StringContent(JsonConvert.SerializeObject(result), System.Text.Encoding.UTF8, "application/json");

            return await Task.FromResult<HttpResponseMessage>(response);
        }

        [HttpPost]
        [Route("GetRole")]
        public async Task<HttpResponseMessage> GetRole(APIRequest request)
        {
            APIResponse result = new APIResponse();
            var response = this.Request.CreateResponse(HttpStatusCode.OK);

            try
            {
                SettingRepository _repository = new SettingRepository();
                int roleId = 0;

                int.TryParse(request.RequestString, out roleId);
                Role role = await _repository.GetRoleAsync(roleId);

                if (role != null)
                {
                    List<Module> moduleList = await _repository.GetModuleListAsync();

                    if (moduleList != null && moduleList.Count > 0)
                    {
                        RoleViewModel model = new RoleViewModel
                        {
                            Id = role.Id,
                            AllowPermission = role.AllowPermission,
                            Description = role.Description,
                            Name = role.Name,
                            ModuleList = new List<ModuleViewModel>(),
                        };

                        foreach (Module module in moduleList)
                        {
                            Module selectedModule = role.Modules.FirstOrDefault(x => x.Id == module.Id);
                            model.ModuleList.Add(new ModuleViewModel
                            {
                                Id = module.Id,
                                ModuleId = module.ModuleId,
                                ModuleName = module.ModuleName,
                                IsSelected = selectedModule != null ? true : false,
                            });
                        }
                        result.RoleList = new List<RoleViewModel>();
                        result.RoleList.Add(model);

                        result.Message = "Got It";
                        result.Code = 1;
                        result.Succeeded = true;
                    }
                    else
                    {
                        result.Message = "No module found in system.";
                    }
                }
                else
                {
                    result.Message = "Role is not found in the system.";
                }

            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Type: API, Controller: Setting, Action: GetRole, Message: {0} ", ex.Message));
                result.Message = ex.Message;
            }

            response.Content = new StringContent(JsonConvert.SerializeObject(result), System.Text.Encoding.UTF8, "application/json");

            return await Task.FromResult<HttpResponseMessage>(response);
        }

        [HttpPost]
        [Route("UpdateRole")]
        public async Task<HttpResponseMessage> UpdateRole(APIRequest request)
        {
            APIResponse result = new APIResponse();
            var response = this.Request.CreateResponse(HttpStatusCode.OK);

            try
            {
                SettingRepository _repository = new SettingRepository();

                string dbResult = await _repository.UpdateRoleAsync(request);
                if (dbResult != "0")
                {
                    result.Code = 1;
                    result.Succeeded = true;
                    result.Message = "New Role sucessfully updated";
                }
                else
                {
                    result.Code = -1;
                    result.Succeeded = false;
                    result.Message = _repository.GetErrorMsg();
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Type: API, Controller: Setting, Action: UpdateRole, Message: {0} ", ex.Message));
                response = this.Request.CreateResponse(HttpStatusCode.InternalServerError);
            }

            response.Content = new StringContent(JsonConvert.SerializeObject(result), System.Text.Encoding.UTF8, "application/json");

            return await Task.FromResult<HttpResponseMessage>(response);
        }

        [HttpPost]
        [Route("UpdateUser")]
        public async Task<HttpResponseMessage> UpdateUser(APIRequest request)
        {
            APIResponse result = new APIResponse();
            var response = Request.CreateResponse(HttpStatusCode.OK);
            try
            {
                SettingRepository _repository = new SettingRepository();
                int dbResult = await _repository.UpdateUserAsync(request);

                if (dbResult > 0)
                {
                    result.Code = 1;
                    result.Succeeded = true;
                    result.Message = "New User sucessfully created";
                }
                else
                {
                    result.Code = -1;
                    result.Succeeded = false;
                    result.Message = _repository.GetErrorMsg();
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Type: API, Controller: Setting, Action: UpdateUser, Message: {0} ", ex.Message));
                response = this.Request.CreateResponse(HttpStatusCode.InternalServerError);
            }

            response.Content = new StringContent(JsonConvert.SerializeObject(result), System.Text.Encoding.UTF8, "application/json");

            return await Task.FromResult<HttpResponseMessage>(response);
        }

        [HttpPost]
        [Route("AddUser")]
        public async Task<HttpResponseMessage> AddUser(APIRequest request)
        {
            APIResponse result = new APIResponse();
            var response = Request.CreateResponse(HttpStatusCode.OK);
            try
            {
                SettingRepository _repository = new SettingRepository();
                int dbResult = await _repository.AddUserAsync(request);

                if (dbResult > 0)
                {
                    result.Code = 1;
                    result.Succeeded = true;
                    result.Message = "New User sucessfully created";
                }
                else
                {
                    result.Code = -1;
                    result.Succeeded = false;
                    result.Message = _repository.GetErrorMsg();
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Type: API, Controller: Setting, Action: AddUser, Message: {0} ", ex.Message));
                response = this.Request.CreateResponse(HttpStatusCode.InternalServerError);
            }

            response.Content = new StringContent(JsonConvert.SerializeObject(result), System.Text.Encoding.UTF8, "application/json");

            return await Task.FromResult<HttpResponseMessage>(response);
        }

        [HttpPost]
        [Route("DeleteUser")]
        public async Task<HttpResponseMessage> DeleteUser(APIRequest request)
        {
            APIResponse result = new APIResponse();
            var response = this.Request.CreateResponse(HttpStatusCode.OK);

            try
            {
                SettingRepository _repository = new SettingRepository();
                result.Code = await _repository.DeleteUserAsync(request);

                if (result.Code > 0)
                {
                    result.Succeeded = true;
                    result.Message = "User sucessfully deleted.";
                }
                else
                {
                    result.Succeeded = false;
                    result.Message = _repository.GetErrorMsg();
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Type: API, Controller: Setting, Action: DeleteUser, Message: {0} ", ex.Message));
                response = this.Request.CreateResponse(HttpStatusCode.InternalServerError);
            }

            response.Content = new StringContent(JsonConvert.SerializeObject(result), System.Text.Encoding.UTF8, "application/json");

            return await Task.FromResult<HttpResponseMessage>(response);
        }


        [HttpPost]
        [Route("AddRole")]
        public async Task<HttpResponseMessage> AddRole(APIRequest request)
        {
            APIResponse result = new APIResponse();
            var response = this.Request.CreateResponse(HttpStatusCode.OK);

            try
            {
                SettingRepository _repository = new SettingRepository();
                string dbResult = await _repository.AddRoleAsync(request);

                if (dbResult != "0")
                {
                    result.Code = 1;
                    result.Succeeded = true;
                    result.Message = "New Role sucessfully created";
                }
                else
                {
                    result.Code = -1;
                    result.Succeeded = false;
                    result.Message = _repository.GetErrorMsg();
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Type: API, Controller: Setting, Action: AddRole, Message: {0} ", ex.Message));
                response = this.Request.CreateResponse(HttpStatusCode.InternalServerError);
            }

            response.Content = new StringContent(JsonConvert.SerializeObject(result), System.Text.Encoding.UTF8, "application/json");

            return await Task.FromResult<HttpResponseMessage>(response);
        }

        [HttpPost]
        [Route("DeleteRole")]
        public async Task<HttpResponseMessage> DeleteRole(APIRequest request)
        {
            APIResponse result = new APIResponse();
            var response = this.Request.CreateResponse(HttpStatusCode.OK);

            try
            {
                SettingRepository _repository = new SettingRepository();
                result.Code = await _repository.DeleteRoleAsync(request);

                if (result.Code > 0)
                {
                    result.Succeeded = true;
                    result.Message = "Role sucessfully deleted.";
                }
                else
                {
                    result.Succeeded = false;
                    result.Message = _repository.GetErrorMsg();
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Type: API, Controller: Setting, Action: DeleteRole, Message: {0} ", ex.Message));
                response = this.Request.CreateResponse(HttpStatusCode.InternalServerError);
            }

            response.Content = new StringContent(JsonConvert.SerializeObject(result), System.Text.Encoding.UTF8, "application/json");

            return await Task.FromResult<HttpResponseMessage>(response);
        }

        [HttpPost]
        [Route("GetRoleList")]
        public async Task<HttpResponseMessage> GetRoleList(APIRequest request)
        {
            APIResponse result = new APIResponse();
            var response = this.Request.CreateResponse(HttpStatusCode.OK);

            try
            {
                SettingRepository _repository = new SettingRepository();
                List<Role> roleList = await _repository.GetRoleListAsync(request.UserName);

                if (roleList != null && roleList.Count > 0)
                {
                    result.RoleList = new List<Core.Model.ViewModel.RoleViewModel>();
                    foreach (Role role in roleList)
                    {
                        result.RoleList.Add(new Core.Model.ViewModel.RoleViewModel
                        {
                            Id = role.Id,
                            Name = role.Name,
                            Description = role.Description,
                            AllowPermission = role.AllowPermission,
                        });
                    }

                    result.Message = "Success.";
                }

            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Type: API, Controller: Setting, Action: GetRoleList, Message: {0} ", ex.Message));
                result.Message = ex.Message;
            }

            response.Content = new StringContent(JsonConvert.SerializeObject(result), System.Text.Encoding.UTF8, "application/json");

            return await Task.FromResult<HttpResponseMessage>(response);
        }

        [HttpPost]
        [Route("GetUserList")]
        public async Task<HttpResponseMessage> GetUserList(APIRequest request)
        {
            APIResponse result = new APIResponse();
            var response = this.Request.CreateResponse(HttpStatusCode.OK);

            try
            {
                SettingRepository _repository = new SettingRepository();
                List<User> userList = await _repository.GetUserListAsync(request.UserName);
                if (userList != null && userList.Count > 0)
                {
                    result.UserList = new List<UserViewModel>();
                    foreach (User user in userList)
                    {
                        result.UserList.Add(new UserViewModel
                        {
                            Id = user.Id,
                            Email = user.Email,
                            UserName = user.UserName,
                            RoleItems = GetRoleDelimitedString(user.Roles),
                            DepartmentItems = GetDepartmentDelimitedString(user.Departments),
                        });
                    }

                    result.Code = 1;
                    result.Message = "Got It";
                    result.Succeeded = true;
                }

            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Type: API, Controller: Setting, Action: GetUserList, Message: {0} ", ex.Message));
                result.Message = ex.Message;
            }

            response.Content = new StringContent(JsonConvert.SerializeObject(result), System.Text.Encoding.UTF8, "application/json");

            return await Task.FromResult<HttpResponseMessage>(response);
        }

        [HttpPost]
        [Route("GetRoleDepartmentList")]
        public async Task<HttpResponseMessage> GetRoleDepartmentList(APIRequest request)
        {
            APIResponse result = new APIResponse();
            var response = this.Request.CreateResponse(HttpStatusCode.OK);

            try
            {
                SettingRepository _repository = new SettingRepository();
                List<Role> roleList = await _repository.GetActiveRoleListAsync();
                if (roleList != null && roleList.Count > 0)
                {
                    List<Department> deptList = await _repository.GetActiveDepartmentListAsync();

                    if (deptList != null && deptList.Count > 0)
                    {
                        // process role list here
                        // process Department list here

                        result.RoleList = new List<Core.Model.ViewModel.RoleViewModel>();
                        foreach (Role role in roleList)
                        {
                            result.RoleList.Add(new Core.Model.ViewModel.RoleViewModel
                            {
                                Id = role.Id,
                                Name = role.Name,
                                Description = role.Description,
                                AllowPermission = role.AllowPermission,
                            });
                        }

                        result.DepartmentList = new List<Core.Model.ViewModel.DepartmentViewModel>();
                        foreach (Department dept in deptList)
                        {
                            result.DepartmentList.Add(new Core.Model.ViewModel.DepartmentViewModel
                            {
                                Department_ID = dept.Department_ID,
                                Department_Name = dept.Department_Name,
                                Description = dept.Description
                            });
                        }

                        result.Code = 1;
                        result.Message = "Got It";
                        result.Succeeded = true;
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Type: API, Controller: Setting, Action: GetRoleDepartmentList, Message: {0} ", ex.Message));
                result.Message = ex.Message;
            }

            response.Content = new StringContent(JsonConvert.SerializeObject(result), System.Text.Encoding.UTF8, "application/json");

            return await Task.FromResult<HttpResponseMessage>(response);

        }

        [HttpPost]
        [Route("GetUser")]
        public async Task<HttpResponseMessage> GetUser(APIRequest request)
        {
            APIResponse result = new APIResponse();
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            try
            {
                SettingRepository _repository = new SettingRepository();
                int userId = 0;
                int.TryParse(request.RequestString, out userId);
                User user = await _repository.GetUserAsync(userId);
                
                if (user != null)
                {

                    UserViewModel userModel = new UserViewModel
                    {
                        Id = user.Id,
                        UserName = user.UserName,
                        Email = user.Email
                    };

                    List<Role> roleList = await _repository.GetActiveRoleListAsync();
                    userModel.RoleList = new List<RoleViewModel>();
                    foreach (Role role in roleList)
                    {
                        userModel.RoleList.Add(new Core.Model.ViewModel.RoleViewModel
                        {
                            Id = role.Id,
                            Name = role.Name,
                            Description = role.Description,
                            AllowPermission = role.AllowPermission,
                            IsSelected = (user.Roles != null && user.Roles.Count > 0 && user.Roles.FirstOrDefault(x => x.Id == role.Id) != null) ? true : false,
                        });
                    }

                    List<Department> deptList = await _repository.GetActiveDepartmentListAsync();
                    userModel.DepartmentList = new List<DepartmentViewModel>();
                    foreach (Department dept in deptList)
                    {
                        userModel.DepartmentList.Add(new Core.Model.ViewModel.DepartmentViewModel
                        {
                            Department_ID = dept.Department_ID,
                            Department_Name = dept.Department_Name,
                            Description = dept.Description,
                            IsSelected = (user.Departments != null && user.Departments.Count > 0 && user.Departments.FirstOrDefault(x => x.Department_ID == dept.Department_ID) != null) ? true : false,
                        });
                    }

                    result.UserList = new List<UserViewModel>();
                    result.UserList.Add(userModel);
                    result.Code = 1;
                    result.Message = "Got It";
                    result.Succeeded = true;
                }
                else
                {
                    result.Message = "User not found in system.";
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Type: API, Controller: Setting, Action: GetUser, Message: {0} ", ex.Message));
                result.Message = ex.Message;
            }

            response.Content = new StringContent(JsonConvert.SerializeObject(result), System.Text.Encoding.UTF8, "application/json");

            return await Task.FromResult<HttpResponseMessage>(response);
        }

        private string GetRoleDelimitedString(ICollection<Role> roleList)
        {
            string result = string.Empty;

            foreach (Role role in roleList)
            {
                if (role.IsDeleted)
                    continue;

                string roleName = string.Format("[{0}]", role.Name);

                if (!result.Contains(roleName))
                {
                    result = string.Format("{0}, {1}", result, roleName);
                }
            }

            if (!string.IsNullOrEmpty(result))
            {
                result = result.Substring(2);
            }

            return result;
        }

        private string GetDepartmentDelimitedString(ICollection<Department> departList)
        {
            string result = string.Empty;

            foreach (Department dept in departList)
            {
                if (dept.IsDeleted)
                    continue;

                string deptName = string.Format("[{0}]", dept.Department_Name);

                if (!result.Contains(deptName))
                {
                    result = string.Format("{0}, {1}", result, deptName);
                }
            }

            if (!string.IsNullOrEmpty(result))
            {
                result = result.Substring(2);
            }

            return result;
        }

    }
}
