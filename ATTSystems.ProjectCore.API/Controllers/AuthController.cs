﻿/* Copyright © ATT Systems Pte Ltd. All Rights Reserved
 * Project      : ATTSystems.ProjectCore.API
 * Author       : Mark Tan
 * Created Date : 2019-06-07
 * Updated Date : 
 */

namespace ATTSystems.ProjectCore.API.Controllers
{
    using ATTSystems.Core;
    using ATTSystems.Core.Logger;
    using ATTSystems.Core.Model.HttpModel;
    using ATTSystems.Core.Model.ViewModel;
    using ATTSystems.ProjectCore.DAL.Implementations;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Net.Mail;
    using System.Threading.Tasks;
    using System.Web.Http;

    [RoutePrefix("api/auth")]
    public class AuthController : ApiController
    {
        [HttpPost]
        [Route("ResetSecureCode")]
        public async Task<HttpResponseMessage> ResetSecureCode(APIRequest request)
        {
            APIResponse result = new APIResponse();
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            try
            {
                LoggerHelper.Instance.TraceLog(string.Format("Incoming - ResetSecureCode"));
                int rowId = 0;
                if (int.TryParse(request.RequestString, out rowId))
                {
                    AuthenticationRepository _repository = new AuthenticationRepository();
                    result.Succeeded = await _repository.ResetSecureCode(rowId, request.Message);
                    if (!result.Succeeded)
                    {
                        result.Message = _repository.GetErrorMsg();
                    }
                }
                else
                {
                    LoggerHelper.Instance.TraceLog(string.Format("Failed - ResetSecureCode Invalid User"));
                    result.Message = "Invalid User.";
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Type: API, Controller: Auth, Action: ResetSecureCode, Message: {0} ", ex.Message));
                result.Message = ex.Message;
            }

            response.Content = new StringContent(JsonConvert.SerializeObject(result), System.Text.Encoding.UTF8, "application/json");

            return await Task.FromResult<HttpResponseMessage>(response);
        }

        [HttpPost]
        [Route("GetResetJobRequest")]
        public async Task<HttpResponseMessage> GetResetJobRequest(APIRequest request)
        {
            APIResponse result = new APIResponse();
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            try
            {
                LoggerHelper.Instance.TraceLog(string.Format("Incoming - GetResetJobRequest"));

                AuthenticationRepository _repository = new AuthenticationRepository();
                result.Code = await _repository.GetUserByRowId(request.RequestString);
                if (result.Code > 0)
                {
                    result.Succeeded = true;
                }
                else
                {
                    result.Message = _repository.GetErrorMsg();
                    if (string.IsNullOrEmpty(result.Message))
                    {
                        result.Message = "Invalid request";
                        LoggerHelper.Instance.TraceLog(string.Format("Failed - GetResetJobRequest - Invalid request"));
                    }
                }

                //result.Succeeded = true;
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Type: API, Controller: Auth, Action: GetResetJobRequest, Message: {0} ", ex.Message));
                result.Message = ex.Message;
            }

            response.Content = new StringContent(JsonConvert.SerializeObject(result), System.Text.Encoding.UTF8, "application/json");

            return await Task.FromResult<HttpResponseMessage>(response);
        }

        [HttpPost]
        [Route("RequestAccountReset")]
        public async Task<HttpResponseMessage> RequestAccountReset(APIRequest request)
        {
            APIResponse result = new APIResponse();
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            try
            {
                AuthenticationRepository _repository = new AuthenticationRepository();
                result.Succeeded = await _repository.RequestAccountReset(request);
                if (result.Succeeded)
                {
                    string key = _repository.GetErrorMsg();
                    string url = string.Empty;
                    List<Core.Model.DBModel.Setting> urlSetting = await _repository.GetSettingByType("URL");
                    if (urlSetting != null && urlSetting.Count > 0)
                    {
                        url = urlSetting.FirstOrDefault().Value;
                    }

                    if (!string.IsNullOrEmpty(url))
                    {
                        url += url.EndsWith("/") ? "Auth/SecureReset/" : "/Auth/SecureReset/";
                        url = string.Format("{0}{1}", url, key);

                        List<Core.Model.DBModel.Setting> SMTPList = await _repository.GetSettingByType("SMTP");
                        if (SMTPList != null && SMTPList.Count > 0)
                        {
                            string msg = ResetPasswordSendMail(url, request.RequestString, SMTPList);
                            if (string.IsNullOrEmpty(msg))
                            {
                                result.Succeeded = true;
                            }
                            else
                            {
                                result.Succeeded = false;
                                result.Message = msg;
                            }
                        }
                        else
                        {
                            result.Succeeded = false;
                            result.Message = "Email Details not available.";
                        }
                    }
                    else
                    {
                        result.Succeeded = false;
                        result.Message = "Invalid based URL";
                    }
                }
                else
                {
                    result.Message = _repository.GetErrorMsg();
                }

            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Type: API, Controller: AuthController, Action: GetModuleList, Message: {0} ", ex.Message));
                response = this.Request.CreateResponse(HttpStatusCode.InternalServerError);
            }

            response.Content = new StringContent(JsonConvert.SerializeObject(result), System.Text.Encoding.UTF8, "application/json");

            return await Task.FromResult<HttpResponseMessage>(response);
        }

        [HttpPost]
        [Route("ChangePassword")]
        public async Task<HttpResponseMessage> ChangePassword(ChangePasswordViewModel model)
        {
            APIResponse result = new APIResponse();
            var response = this.Request.CreateResponse(HttpStatusCode.OK);

            try
            {
                AuthenticationRepository _repository = new AuthenticationRepository();
                var user = await _repository.GetUserAsync(model.UserName);


                if (user != null)
                {
                    if (CryptographyHash.VerifyHashedPassword(user.PasswordHash, model.OldPassword))
                    {
                        string msg = await _repository.ChangePasswordAsync(model.UserName, CryptographyHash.HashPassword(model.NewPassword));

                        if (string.IsNullOrEmpty(msg))
                        {
                            result.Succeeded = true;
                            result.Message = "Password successfully changed.";
                            result.Code = 1;
                        }
                        else
                        {
                            result.Succeeded = false;
                            result.Message = msg;
                            result.Code = -3;
                        }
                    }
                    else
                    {
                        result.Succeeded = false;
                        result.Message = "User's old password does not match";
                        result.Code = -2;
                    }

                }
                else
                {

                    result.Succeeded = false;
                    result.Message = "User is not found in the system";
                    result.Code = -1;
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Type: API, Controller: Auth, Action: ChangePassword, Message: {0} ", ex.Message));
            }

            response.Content = new StringContent(JsonConvert.SerializeObject(result), System.Text.Encoding.UTF8, "application/json");

            return await Task.FromResult<HttpResponseMessage>(response);
        }

        [HttpPost]
        [Route("Authentication")]
        public async Task<HttpResponseMessage> Authentication(LoginViewModel loginViewModel)
        {
            LoggerHelper.Instance.TraceLog("Incoming request");
            string userName = loginViewModel != null && !string.IsNullOrEmpty(loginViewModel.Username) ? loginViewModel.Username : string.Empty;
            AuthResponse resp = new AuthResponse
            {
                IsAuth = false,
                AuthResult = false,
                DepartmentList = null,
                RoleList = null,
                ErrorMessage = "Internal error!",
                UserId = userName,

            };

            var response = this.Request.CreateResponse(HttpStatusCode.OK);

                try
            {
                // check login password format
                AuthenticationRepository _repository = new AuthenticationRepository();

                // Query DB Here
                var user = await _repository.GetUserAsync(userName);
                resp.IsAuth = true;

                if (user != null)
                {
                    if (CryptographyHash.VerifyHashedPassword(user.PasswordHash, loginViewModel.Password))
                    {
                        resp.RoleList = ConsolidateRoles(user.Roles); //user.Roles != null ? user.Roles.ToList() : null;
                        resp.DepartmentList = ConsolidateDepartment(user.Departments); //user.Departments != null ? user.Departments.ToList() : null;
                        resp.AuthResult = true;
                        resp.ErrorMessage = "OK";
                        resp.ModuleList = ConsolidateModule(user.ModuleList);  //user.ModuleList != null ? user.ModuleList : null;
                    }
                    else
                    {
                        resp.ErrorMessage = "Invalid user password!";
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(_repository.GetErrorMsg()))
                    {
                        resp.ErrorMessage = "Invalid user name!";
                    }
                    else
                    {
                        resp.ErrorMessage = _repository.GetErrorMsg();
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Type: API, Controller: Auth, Action: Authentication, Message: {0} ", ex.Message));
            }

            response.Content = new StringContent(JsonConvert.SerializeObject(resp), System.Text.Encoding.UTF8, "application/json");

            return await Task.FromResult<HttpResponseMessage>(response);

        }

        [HttpPost]
        [Route("GetModuleList")]
        public async Task<HttpResponseMessage> GetModuleList(APIRequest request)
        {
            APIResponse result = new APIResponse();
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            try
            {
                AuthenticationRepository _repository = new AuthenticationRepository();
                List<ModuleViewModel> moduleList = await _repository.GetModuleList(request);
                if (moduleList != null && moduleList.Count > 0)
                {
                    result.Succeeded = true;
                    result.ModuleList = moduleList;
                }

            }
            catch (Exception ex)
            {
                LoggerHelper.Instance.TraceLog(string.Format("Level: Warning, Type: API, Controller: AuthController, Action: GetModuleList, Message: {0} ", ex.Message));
                response = this.Request.CreateResponse(HttpStatusCode.InternalServerError);
            }

            response.Content = new StringContent(JsonConvert.SerializeObject(result), System.Text.Encoding.UTF8, "application/json");

            return await Task.FromResult<HttpResponseMessage>(response);
        }

        private string ResetPasswordSendMail(string url, string receipentEmail, List<Core.Model.DBModel.Setting> SMTPList)
        {
            string result = string.Empty;
            try
            {
                LoggerHelper.Instance.TraceLog(string.Format("ResetPasswordSendMail 1 ------ Started"));
                int port = 25;
                MailMessage msg = new MailMessage();
                msg.To.Add(new MailAddress(receipentEmail));
                msg.Subject = "Reset your Account Password";
                msg.Body = "Dear User,<br><br>You have requested to reset your password, please click <a href='" + url + "'>HERE</a> or copy the following link to your browser.<br><br>";
                msg.Body += url;
                msg.Body += "<br>This link will expired in next 15 minutes. If you're still having trouble resetting your password, please contact oour staff.";
                msg.Body += "<br><br>Sincerely,<br>VMS Team<br><br>Please do not reply to this message.";
                msg.IsBodyHtml = true;
                msg.From = new MailAddress(SMTPList.FirstOrDefault(x => x.Field == "SenderEmail").Value);
                string strPort = SMTPList.FirstOrDefault(x => x.Field == "SMTPPort").Value;
                if (!int.TryParse(strPort, out port))
                {
                    port = 25;
                }

                string replyAddr = SMTPList.FirstOrDefault(x => x.Field == "ReplyEmail").Value;
                if (!string.IsNullOrEmpty(replyAddr))
                {
                    msg.ReplyToList.Add(replyAddr);
                }
                else
                {
                    msg.ReplyToList.Add(msg.From);
                }


                SmtpClient smtp = new SmtpClient();
                LoggerHelper.Instance.TraceLog(string.Format("ResetPasswordSendMail 2."));
                smtp.Host = SMTPList.FirstOrDefault(x => x.Field == "SMTPServer").Value;
                smtp.Port = port;
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new System.Net.NetworkCredential(SMTPList.FirstOrDefault(x => x.Field == "SMTPUserId").Value, SMTPList.FirstOrDefault(x => x.Field == "SMTPPassword").Value);
                smtp.Send(msg);
                msg.Dispose();
                smtp.Dispose();

                LoggerHelper.Instance.TraceLog(string.Format("ResetPasswordSendMail completed."));

            }
            catch (Exception ex)
            {
                result = ex.Message;
                LoggerHelper.Instance.TraceLog(string.Format("[ERROR] SendEmail - Report: ErrMsg:{0}.", ex.Message));
            }

            return result;
        }

        private List<RoleViewModel> ConsolidateRoles(ICollection<Core.Model.DBModel.Role> roleList)
        {
            List<RoleViewModel> result = null;
            //user.Roles != null ? user.Roles.ToList() : null;
            if (roleList != null)
            {
                result = new List<RoleViewModel>();
                foreach (var item in roleList)
                {
                    result.Add(new RoleViewModel
                    {
                        Id = item.Id,
                        AllowPermission = item.AllowPermission,
                        Description = item.Description,
                        Name = item.Name,
                    });
                }
            }

            return result;
        }

        private List<DepartmentViewModel> ConsolidateDepartment(ICollection<Core.Model.DBModel.Department> departList)
        {
            List<DepartmentViewModel> result = null;

            if (departList != null)
            {
                result = new List<DepartmentViewModel>();
                foreach (var item in departList)
                {
                    result.Add(new DepartmentViewModel
                    {
                        Department_ID = item.Department_ID,
                        Department_Name = item.Department_Name,
                        Description = item.Description
                    });
                }
            }

            return result;
        }

        private List<ModuleViewModel> ConsolidateModule(ICollection<Core.Model.DBModel.Module> moduleList)
        {
            List<ModuleViewModel> result = null;
            if (moduleList != null)
            {
                result = new List<ModuleViewModel>();
                foreach (var item in moduleList)
                {
                    result.Add(new ModuleViewModel
                    {
                        Id = item.Id,
                        ModuleId = item.ModuleId,
                        ModuleName = item.ModuleName
                    });
                }
            }

            return result;
        }



    }
}
